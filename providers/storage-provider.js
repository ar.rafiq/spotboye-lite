function Storage() {
    var DBNAME = "SpotboyE-cache"
    var _idb = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    var transaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    var keyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

    var db;
    var request = _idb.open("SpotboyE-cacheDB");
    var OS_Dashbaord = "dashboard";
    var OS_Menus = "menus";
    request.onerror = function(event) {
        console.log("Cache db access cancelled");
    };
    request.onsuccess = function(event) {
        db = event.target.result;
    };
    request.onupgradeneeded = function(event) {
        var db = event.target.result;

        db.createObjectStore(OS_Dashbaord, {
            keyPath: "_id"
        });
        db.createObjectStore(OS_Menus, {
            keyPath: "_id"
        });
    };

    this.isSupported=function() {
        if (!_idb) {
            return false;
        } else {
            return true;
        }
    }

    this.saveDashboardList=function(list,cb) {
        var transaction = db.transaction([OS_Dashbaord], "readwrite");
        transaction.oncomplete = function(event) {
            console.log("All done!");
            cb();
        };

        transaction.onerror = function(event) {};

        var objectStore = transaction.objectStore(OS_Dashbaord);
        for (var i in list) {
            var request = objectStore.add(list[i]);
            request.onsuccess = function(event) {
                console.log("Added article ", list[i]._id);
                // event.target.result == customerData[i].ssn;
            };
        }

    }

    this.getDashboardList=function(page, limit) {

    }

    var _user_info={islogged:false,name:"",profilePic:"",userId:""};

    this.getCurrentUser=function(){
        var _info=localStorage.getItem("USER_INFO");
        if(_info&&JSON.parse(_info)){
            return JSON.parse(_info);
        }else{
            return _user_info;
        }
    }
    this.clearUser=function(){
      localStorage.setItem("USER_INFO",JSON.stringify({islogged:false,name:"",profilePic:"",userId:""}));
    }
    this.saveUserSession=function(data){
       return localStorage.setItem("USER_INFO",JSON.stringify({islogged:true,name:data.name,profilePic:data.profilePic,userId:data.authToken}));
    }
    this.saveUserDetails=function(key,val){
        var _t=this.getCurrentUser();
        _t[key]=val;
        localStorage.setItem("USER_INFO",JSON.stringify(_t));
    }

}
