var APIService = function (Storage, $http) {
	var getLang=function(){
	return (localStorage.getItem('locale') && ['en', 'hi'].indexOf(localStorage.getItem('locale')) != -1) ? localStorage.getItem('locale'):'';
}
	this.getMenus = function (cb) {
		$http.get(WEBSERVICE.URI.getURI('menus')).then(function (res) {
			if (res.status == 200) {
				cb(res.data.data);
			}
			else {
				cb([]);
			}
		}, function () {
			cb([]);
		})
	}
	this.getDashboard = function (cb) {
		$http.get(WEBSERVICE.URI.getURI('dashboard', {}, getLang())).then(function (res) {
			if (res.status == 200) {
				cb(res.data.data);
			}
			else {
				cb([]);
			}
		}, function () {
			cb([]);
		})
	}
	this.getArticle = function (param, cb) {
		var _did = '';
		if (localStorage.getItem('deviceId')) {
			_did = "&deviceId=" + localStorage.getItem('deviceId');
		}
		if (param.userId) {
			_did = '&userId=' + param.userId
		}
		
		$http.get(WEBSERVICE.URI.getURI('article', param, getLang()) + _did).then(function (res) {
			if(res.data.data._id&&res.data.data._id!=""&&res.data.data._id!=0){
				res.data.data.success=true;
			cb(res.data.data);
			}else{
				cb({success:false});
			}
		});
	}
	this.getLikes = function (param, cb) {
		var _u = '';
		if (Storage.getCurrentUser().islogged) {
			_u = '&userId=' + Storage.getCurrentUser().userId;
		}
		$http.get(WEBSERVICE.URI.getURI('likes', param) + _u).then(function (res) {
			cb(res.data.data);
		});
	}
	this.doLogin = function (data, cb) {
		$http.post(WEBSERVICE.URI.getURI('login'), data).then(function (res) {
			Storage.saveUserSession(res.data.data);
			cb({
				success: true
			});
		}).catch(function (res) {
			console.log("ERRROR RES", res);
			cb({
				success: false
				, message: (res.data.errors.message) ? res.data.errors.message : "Unable to login",
				isVerified:(res.data.errors.isValid) ? 1:0
			});
		});
	}
	this.doFacebookLogin = function (data, cb) {
		$http.post(WEBSERVICE.URI.getURI('facebook_login'), data).then(function (res) {
			Storage.saveUserSession(res.data.data);
			cb({
				success: true
			});
		}).catch(function (res) {
			cb({
				success: false
				, message: (res.data.errors.message) ? res.data.errors.message : "Unable to login",
				isVerified:(res.data.errors.isValid) ? 1:0
			});
		});
	}
	this.getMyProfile = function (_p, cb) {
		$http.post(WEBSERVICE.URI.getURI('my_profile'), _p).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getUserProfile = function (param, cb) {
		$http.post(WEBSERVICE.URI.getURI('user_profile'), param).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.search = function (keyword, cb) {
		$http.get(WEBSERVICE.URI.getURI('search', keyword, getLang())).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getByCategory = function (params, cb) {
		$http.get(WEBSERVICE.URI.getURI('category', params, getLang())).then(function (res) {

			if(res.data&&res.data.data.length>0){
				res.data.success=true;
			cb(res.data);
		}else{
			cb({
				success: false
			});
		}
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getBySubCategory = function (params, cb) {
		$http.get(WEBSERVICE.URI.getURI('subcategory', params, getLang())).then(function (res) {
			cb(res.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getPhotoGallery = function (params, cb) {
		$http.get(WEBSERVICE.URI.getURI('photo_gallery', params)).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getCelebrity = function (params, cb) {
		var _qry='';
		if(params.userId){
			_qry='?userId='+params.userId;
		}

		$http.get(WEBSERVICE.URI.getURI('celebrity', {celebrityId:params.celebrityId})+_qry).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getCelebrities = function (cb) {
		$http.get(WEBSERVICE.URI.getURI('celebrities')).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.findCelebs = function (_p, cb) {
		$http.get(WEBSERVICE.URI.getURI('celebrity_search', _p)).then(function (res) {
			if(res.data.data){
				res.data.data.success=true;
			}
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.signup = function (params, cb) {
		$http.post(WEBSERVICE.URI.getURI('signup'), params).then(function (res) {
			if (res.data.errors) {
				cb({
					success: false
					, message: (res.data.errors.message) ? res.data.errors.message : "Error occurred"
				});
			}
			else {
				cb({
					success: true
					, data: res.data.data
				});
			}
		}).catch(function (res) {
			cb({
				success: false
				, message: res.data.errors.message
			});
		});
	}
	this.verifyOTP = function (params, cb) {
		$http.post(WEBSERVICE.URI.getURI('verify_signup'), params).then(function (res) {
			if (res.data.errors) {
				cb({
					success: false
					, message: res.data.errors.message
				});
			}
			else {
				Storage.saveUserSession(res.data.data);
				cb({
					success: true
					, data: res.data.data
				});
			}
		}).catch(function (res) {
			console.log(res, "error");
			cb({
				success: false
				, message: res.data.errors.message
			});
		});
	}
	this.getSidebar = function (params, cb) {
		var _did = '';
		var user = Storage.getCurrentUser();
		if (user.islogged) {
			_did = "?userId=" + user.userId;
		}
		else if (localStorage.getItem('deviceId')) {
			_did = "?deviceId=" + localStorage.getItem('deviceId');
		}
		$http.get(WEBSERVICE.URI.getURI('sidebar', params, getLang()) + _did).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getSubmenuArticle = function (params, cb) {
		$http.get(WEBSERVICE.URI.getURI('submenu_article', params, getLang())).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getNextArticle = function (params, cb) {
		$http.get(WEBSERVICE.URI.getURI('next_article', params, getLang())).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.likeArticle = function (params, cb) {
		$http.post(WEBSERVICE.URI.getURI('like_article'), params).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.setFollow = function (params, cb) {
		$http.post(WEBSERVICE.URI.getURI('follow'), params).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.setCelebrityFollow = function (params, cb) {
		$http.post(WEBSERVICE.URI.getURI('celebrity_follow'), params).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getSocialFeed = function (params, cb) {
		$http.get(WEBSERVICE.URI.getURI('feed', params)).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.poll = function (params, cb) {
		var _did = '';
		if (localStorage.getItem('deviceId')) {
			params.deviceId = localStorage.getItem('deviceId');
		}
		$http.post(WEBSERVICE.URI.getURI('poll'), params).then(function (res) {
			if (res.data.data.deviceId) {
				localStorage.setItem("deviceId", res.data.data.deviceId);
			}
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}
	this.getVideoDetails = function (params, cb) {
		$http.get(WEBSERVICE.URI.getURI('article', params)).then(function (res) {
			cb(res.data.data);
		}).catch(function (res) {
			cb({
				success: false
			});
		});
	}

	this.resetPassword = function (params, cb) {
		$http.post(WEBSERVICE.URI.getURI('reset_password'), params).then(function (res) {
			if (res.data.errors) {
				cb({
					success: false
					, message: res.data.errors.message
				});
			}
			else {

				cb({
					success: true
				});
			}
		}).catch(function (res) {
			console.log(res, "error");
			cb({
				success: false
				, message: res.data.errors.message
			});
		});
	}
this.resendOTP= function (params, cb) {
	$http.post(WEBSERVICE.URI.getURI('resend_otp'), params).then(function (res) {
		if (res.data.errors) {
			cb({
				success: false
				, message: res.data.errors.message
			});
		}
		else {

			cb({
				success: true
			});
		}
	}).catch(function (res) {
		cb({
			success: false
			, message: res.data.errors.message
		});
	});
}
this.getUpcomingMovies = function (cb) {
	$http.get("https://apiv2.spotboye.com/upcoming-movies").then(function (res) {
		if (res.status == 200) {
			cb(res.data);
		}
		else {
			cb(null);
		}
	}, function () {
		cb(null);
	})
}

	return {
		get: this.get
		, getMenus: this.getMenus
		, getDashboard: this.getDashboard
		, getArticle: this.getArticle
		, doLogin: this.doLogin
		, signup: this.signup
		, getMyProfile: this.getMyProfile
		, getUserProfile: this.getUserProfile
		, search: this.search
		, getByCategory: this.getByCategory
		, getBySubCategory: this.getBySubCategory
		, getPhotoGallery: this.getPhotoGallery
		, verifyOTP: this.verifyOTP
		, getSidebar: this.getSidebar
		, getSubmenuArticle: this.getSubmenuArticle
		, getNextArticle: this.getNextArticle
		, likeArticle: this.likeArticle
		, setFollow: this.setFollow
		, getSocialFeed: this.getSocialFeed
		, getCelebrity: this.getCelebrity
		, getCelebrities: this.getCelebrities,
		setCelebrityFollow:this.setCelebrityFollow
		, poll: this.poll
		, doFacebookLogin: this.doFacebookLogin
		, getLikes: this.getLikes
		, findCelebs: this.findCelebs,
		resetPassword:this.resetPassword,
		resendOTP:this.resendOTP,
		getUpcomingMovies:this.getUpcomingMovies
	}
}
