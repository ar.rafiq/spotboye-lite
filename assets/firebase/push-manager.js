// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();

function sendTokenToServer(token) {
    var deviceId=(localStorage.getItem("deviceId")==null)?"":localStorage.getItem("deviceId");
    fetch("https://apiv2.spotboye.com/registerDevice", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            platform: "web",
            deviceId: deviceId,
            deviceToken: token
        })
    }).then(res=>res.json()).then(res=>{
        console.log(res,"APIV2 RES")
        if(res.data.deviceId){
            localStorage.setItem("deviceId",res.data.deviceId);
        }
    }).catch(err=>console.log(err,"FCM SERVER ERR"));
    console.log(token, "TOKEN");
}

messaging.requestPermission().then(function () {
    console.log('Notification permission granted.');
    if (localStorage.getItem("ftoken") == null || localStorage.getItem("ftoken") == "") {
        messaging.getToken().then(function (currentToken) {
            if (currentToken) {
                sendTokenToServer(currentToken);
                localStorage.setItem("ftoken", currentToken);
            } else {
                console.log(
                    'No Instance ID token available. Request permission to generate one.');
            }
        }).catch(function (err) {
            console.log("CATCH", err);
        });

    }
}).catch(function (err) {
    console.log('Unable to get permission to notify.', err);
});
messaging.onMessage(function (payload) {
    console.log('Message received. ', payload);
    var notificationTitle = payload.data.title;
    var notificationOptions = {
        title:payload.data.title,
      body: payload.data.body,
      icon: payload.data.image
    };
  
    var notification = new Notification(notificationTitle,notificationOptions);
    notification.onclick = function(event) {
        event.preventDefault(); // prevent the browser from focusing the Notification's tab
        window.open(payload.data.click_action , '_blank');
        notification.close();
    }
});

messaging.onTokenRefresh(function () {
    messaging.getToken().then(function (refreshedToken) {
        console.log('Token refreshed.', refreshedToken);
        sendTokenToServer(refreshedToken);
    }).catch(function (err) {
        console.log('Unable to retrieve refreshed token ', err);
    });
});