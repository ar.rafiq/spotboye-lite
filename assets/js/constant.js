var WEBSERVICE={};
WEBSERVICE.HOSTNAME="https://apiv2.spotboye.com";
WEBSERVICE.DEVICE="desktop";
WEBSERVICE.ROUTE={};
WEBSERVICE.ROUTE.menus="/appMenus";
WEBSERVICE.ROUTE.dashboard="/dashboardWeb";
WEBSERVICE.ROUTE.article="/articleDetailsWeb?deviceType="+WEBSERVICE.DEVICE+"&articleId=:article_id";
WEBSERVICE.ROUTE.login="/auth/email/login";
WEBSERVICE.ROUTE.reset_password="/resetPassword";
WEBSERVICE.ROUTE.resend_otp="/resendVerifyCode";
WEBSERVICE.ROUTE.facebook_login="/auth/facebook/token";
WEBSERVICE.ROUTE.signup="/users";
WEBSERVICE.ROUTE.verify_signup="/auth/verifyOtp";
WEBSERVICE.ROUTE.my_profile="/userProfile";
WEBSERVICE.ROUTE.user_profile="/otherUserProfile";
WEBSERVICE.ROUTE.search="/search/:param";
WEBSERVICE.ROUTE.category="/category/:param";
WEBSERVICE.ROUTE.sidebar="/sidebar/:param";
WEBSERVICE.ROUTE.subcategory="/category/:parent/:child?page=:page&limit=:limit";
WEBSERVICE.ROUTE.submenu_article="/category/:parent/:child?limit=3&submenu=1";
WEBSERVICE.ROUTE.photo_gallery="/photoDetails?photoId=:param";
WEBSERVICE.ROUTE.next_article="/article/:id/next?catId=:catId";
WEBSERVICE.ROUTE.like_article="/likeArticle";
WEBSERVICE.ROUTE.likes="/likeList?articleId=:param";
WEBSERVICE.ROUTE.follow="/userFollow";
WEBSERVICE.ROUTE.feed="/social/:param";
WEBSERVICE.ROUTE.poll="/article/userAnswer";

WEBSERVICE.ROUTE.celebrity="/celebrities/:celebrityId";
WEBSERVICE.ROUTE.celebrity_follow="/celebrityFollow";
WEBSERVICE.ROUTE.celebrities="/celebrities";
WEBSERVICE.ROUTE.celebrity_search="/celebrities/search/:name";



WEBSERVICE.URI={};
WEBSERVICE.URI.getURI=function(endpoint,param,lang){
    var host_url = (lang) ? WEBSERVICE.HOSTNAME + '/' + lang : WEBSERVICE.HOSTNAME;
    if(WEBSERVICE.ROUTE[endpoint]){
      if(typeof param != "object"){
          return host_url+WEBSERVICE.ROUTE[endpoint].replace(":param",param);
      }else{
          var _url = host_url+WEBSERVICE.ROUTE[endpoint];
        Object.keys(param).forEach(function(v,i){
          _url=_url.replace(":"+v,param[v]);
        });
        return _url;

      }
    }else{
        return host_url;
    }
}


var route=[{
  controller:'dashboardCtrlr',
  path:'/dashboard',
  template:'dashboard.html',
  src:'dashboard.js'
},{
  controller:'articleCtrlr',
  path:'/article/:id',
  template:'article.html',
  src:'article.js'
}];

function getMomentAgo(date) {

  var msPerMinute = 60 * 1000;
     var msPerHour = msPerMinute * 60;
     var msPerDay = msPerHour * 24;
     var msPerMonth = msPerDay * 30;
     var msPerYear = msPerDay * 365;

     var elapsed =(new Date().getTime())-date;// current - previous;

     if (elapsed < msPerMinute) {
          return Math.round(elapsed/1000) + ' sec';
     }

     else if (elapsed < msPerHour) {
          return Math.round(elapsed/msPerMinute) + ' mins';
     }

     else if (elapsed < msPerDay ) {
          return Math.round(elapsed/msPerHour ) + ' hrs';
     }else{

         //SHOW FULL DATE
          var monthNames = [
    "Jan", "Feb", " Mar",
    "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct",
    "Nov", "Dec"
  ];

         var date=new Date(date);
  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
     }

     /*else if (elapsed < msPerMonth) {
         return Math.round(elapsed/msPerDay) + ' days';
     }

     else if (elapsed < msPerYear) {
         return Math.round(elapsed/msPerMonth) + ' months';
     }

     else {
         return Math.round(elapsed/msPerYear ) + ' years';
     }*/


}
function randomBetween(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}
function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');
    if (urlparts.length>=2) {
        return urlparts[0];
    } else {
        return url;
    }
}
