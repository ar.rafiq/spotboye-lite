
var CACHE_NAME = 'spotboye-lite-cache-v2';
var DATA_CACHE_NAME = 'spotboye-lite-dbcache-v2';
var urlsToCache = [
	'/',
	'/category',
	'/app.js',
	'/firebase/init.js',
	'/firebase-messaging-sw.js',
	'/firebase/push-manager.js',
	'/manifest.json',
	'/js/constant.js',
	'/lib/jquery.slim.min.js',
	'/lib/angular.min.js',
	'/lib/angular-ui-router.min.js',
	'/lib/slick/slick.min.js',
	'/lib/slick/angular-slick.min.js"',
	'/providers/api-service.js',
	'/providers/storage-provider.js',
	'/css/bootstrap.min.css',
	'/css/styles-mobile.min.css',	
	'/css/styles.min.css',
	'/css/font-awesome.min.css',	
	'/lib/slick/slick.css',	
	'/lib/slick/slick-theme.min.css'	
]
	
self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open(CACHE_NAME).then(function(cache) {
     return cache.addAll(urlsToCache);
   })
 );
});

self.addEventListener('activate', function(event) {

    console.log('Service Worker: Activating....');

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(cacheNames.map(function(key) {
                if( key !== CACHE_NAME) {
                    console.log('Service Worker: Removing Old Cache', key);
                    return caches.delete(key);
                }
            }));
        })
    );
    return self.clients.claim();
});

self.addEventListener('fetch', function(event) {

    console.log('Service Worker: Fetch', event.request.url);

    console.log("Url", event.request.url);
 event.waitUntil(
   caches.open(CACHE_NAME).then(function(cache) {
     return cache.addAll(event.request.url);
   })
 );
    event.respondWith(
        caches.match(event.request).then(function(response) {
            return response || fetch(event.request);
        })
    );
});


/*self.addEventListener('fetch', function(e) {
  console.log('[Service Worker] Fetch', e.request.url);
		
  var dataUrl = 'https://apiv2.spotboye.com';
  if (e.request.url.indexOf(dataUrl) > -1 || e.request.url.indexOf('http://dev.spotboye.com') > -1|| e.request.url.indexOf('http://localhost:3000') > -1) {
	  	  console.log("if..",e.request.url);

    e.respondWith(
      caches.open(DATA_CACHE_NAME).then(function(cache) {
        return fetch(e.request).then(function(response){
          cache.put(e.request.url, response.clone());
          return response;
        });
      })
    );
  } else {
	  console.log("else..");
    e.respondWith(
      caches.match(e.request).then(function(response) {
        return response || fetch(e.request);
      })
    );
  }
});*/
