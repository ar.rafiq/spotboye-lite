importScripts('https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.0.4/firebase-messaging.js');
importScripts('/firebase/init.js');

//version 0.5
var messaging = firebase.messaging();


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = payload.data.title;
  var notificationOptions = {
    body: payload.data.body,
    icon: payload.data.image,
    data:{url:payload.data.click_action},
    actions: [{action: "open_url", title: "Read Now"},{action: "open_web", title: "Open Website"}]
  };

  
  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});

self.addEventListener('notificationclick', function(event) {
  console.log(event,"rcvd data");
  switch(event.action){
    case 'open_url':
    clients.openWindow(event.notification.data.url);
    break;
    case 'open_web':
    clients.openWindow("https://www.spotboye.com");
    break;
    default:
    clients.openWindow(event.notification.data.url);
  }
}
, false);
fetch('https://apiv2.spotboye.com').then(res=>{
  console.log("fetch init");
});
// [END background_handler]