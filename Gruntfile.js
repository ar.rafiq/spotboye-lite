var path = require('path');

//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

  grunt.initConfig({
uglify: {
  options: {
     mangle: false
   },
    my_target: {
		files:[{
      expand: true,
      src: ['assets/js/*.js','phone/app.js','providers/*.js','assets/lib/*.js','assets/lib/*/*.js'],
      dest: 'production',
      cwd: '.',
      rename: function (dst, src) {
        // To keep the source js files and make new files as `*.min.js`:
         return dst + '/' + src;
		  /*return dst + '/' + src.replace('.js', '.min.js');*/
        // Or to override to src:
       // return src;
      }
    }]

    }
  },
	  cssmin: {
  target: {
    files: [{
      expand: true,
      cwd: '.',
      src: ['assets/css/*.css', '!assets/css/*.min.css'],
      dest: 'production',
      ext: '.min.css'
    }]
  }
},
	   htmlmin: {                                     // Task
    dist: {                                      // Target
      options: {                                 // Target options
        removeComments: true,
        collapseWhitespace: true
      },
      files: [{
          expand: true,
          cwd: '.',
          src: ['phone/*.ejs', 'phone/**/*.html',],
          dest: 'production'
      }]
    }
     }
});

grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-contrib-htmlmin');

grunt.registerTask('makeJS', ['uglify:my_target','cssmin:target','htmlmin']);
};
