var app = angular.module("base", ["ui.router", "slickCarousel", "socialLogin", 'angularLazyImg', 'angularDfp']); //, "rzModule"
var DEVICE_TYPE = "desktop";
var user_info = {
	islogged: false
};
var outerHeight = 0;
var playersVideo = [];

app.run(['$rootScope', '$location', '$window', 'dfp', '$state', function ($rootScope, $location, $window, dfp, $state) {
	// initialise google analytics
	$window.ga('create', 'UA-53522784-1', 'auto');
	//	$window.ga('require', 'GTM-TMRTK62');

	$rootScope.$on('$stateChangeSuccess', function (event) {

		$window.scrollTo(0, 0);
		$window.ga('send', 'pageview', $location.path());
		//tmp
		$('#skiner_video').attr('src', $('iframe').attr('src'));
	});

	$rootScope.locale = (localStorage.getItem('locale')) ? localStorage.getItem('locale') : 'en';
	$rootScope.localeURL = ($rootScope.locale != 'en') ? '/' + $rootScope.locale:'';
	$window.onbeforeunload = function () {
		$window.scrollTo(0, 0);
	}

	// initialize angular-dfp
	dfp();
	$rootScope.changeLocale = function (v) {
		localStorage.setItem('locale', v);
		var _p = (v == 'hi') ? 'hi/' : '';
		window.location = window.location.protocol + "//" + window.location.host + "/" + _p;
	};
}]);
app.service("Storage", Storage);
// app.service("Playground", Playground);
app.factory("APIService", APIService);
app.filter('slug', function () {
	return function (x) {
		if (x && x.length) {
			//return x.replace(/[^a-zA-Z0-9-_]/g, '-').replace(/-+/g, '-').toLowerCase();

			x = x.replace(/[^a-zA-Z0-9-_]/g, '-');
			x = x.replace(/-+/g, '-').toLowerCase();

			if (x[x.length - 1] == '-') {
				x = x.slice(0, x.length - 1);
			}
			return x;
			// if (addArticleData.slug[addArticleData.slug.length - 1] == '-') {
			//     addArticleData.slug = addArticleData.slug.slice(0, addArticleData.slug.length - 1);
			// }
			//return x.replace(/[^a-z A-Z0-9_-]/g, '').replace(/[^a-zA-Z0-9_-]/g, "-").toLowerCase();

		} else {
			return '';
		}
	};
});
app.filter('timeago', function () {
	return function (x) {
		if (x) {
			return getMomentAgo(new Date(x).getTime());
		}
	};
});
app.filter('kViewers', function () {
	return function (x) {
		return (parseInt(x) > 999) ? (parseInt(x) / 1000).toFixed(1) + "K" : x;
	};
});
app.filter('trustUrl', function ($sce) {
	return function (url) {
		return $sce.trustAsResourceUrl(url);
	};
});
app.filter('urlEncode', function () {
	return function (input) {
		if (input) {
			return window.encodeURIComponent(input);
		}
		return "";
	}
});
app.controller('baseCtrl', ['$rootScope', '$scope', '$state', 'Storage', 'APIService', '$filter', '$timeout', '$sce', '$location', '$window', '$stateParams', function ($rootScope, $scope, $state, Storage, APIService, $filter, $timeout, $sce, $location, $window, $stateParams) {
	angular.element('body').scrollTop(0);
	user_info = Storage.getCurrentUser();

		
	$rootScope.openArticle = function (section) {
		var _title = section.slug;// $filter('slug')(section.title);
		$state.go('article', {
			category: section.mainSection.parentSlug,
			subcategory: section.mainSection.slug,
			title: _title,
			id: section._id
		});
	}
	$rootScope.openProfile = function () {
		user_info = Storage.getCurrentUser();
		if (user_info.islogged && user_info.userId) {
			$scope.$emit('showUiBackdrop', {
				type: 'url',
				url: '/desktop/templates/profile.html',
				param: user_info
			})
		} else {
			$scope.$emit('showUiBackdrop', {
				type: 'url',
				url: '/desktop/templates/login.html'
			})
		}
	}
	$scope.stateParams = $stateParams;
	$rootScope.menus = [];
	APIService.getMenus(function (menus) {
		$rootScope.menus = menus;
		$rootScope.$broadcast('onMenuChanged', {
			menus: menus
		});
	});
	var _last_submenu_article = '';
	$scope.submenu_showLoading = false;
	$rootScope.getSubmenuArticle = function (_p, _i) {
		var _par = $rootScope.menus[_p].slug;
		var _sub = $rootScope.menus[_p].subMenu[_i].slug;
		if (!$rootScope.menus[_p].subMenu[_i].lists || $rootScope.menus[_p].subMenu[_i].lists.length == 0) {
			if (_last_submenu_article != $rootScope.menus[_p].subMenu[_i]._id) {
				$scope.submenu_showLoading = true;
				APIService.getSubmenuArticle({
					parent: _par,
					child: _sub
				}, function (_articles) {
					$scope.submenu_showLoading = false;
					$rootScope.menus[_p].subMenu[_i].lists = _articles;
				});
			}
			_last_submenu_article = $rootScope.menus[_p].subMenu[_i]._id;
		}
	}
	$rootScope.getSubMenu = function (category, _return_parent) {
		angular.forEach($rootScope.menus, function (sub, i) {
			if (sub.slug == category) {
				if (_return_parent) {
					return sub;
				}
				return sub.subMenu;
			} else {
				return [];
			}
		});
	}
	$rootScope.getMenu = function (category, cb) {

		angular.forEach($rootScope.menus, function (sub, i) {
			if (sub.slug == category) {
				cb(sub);
			}
		});
	}
	$rootScope.photo = {};
	$rootScope.photo.viewGallery = function (_id) {
		$scope.$emit('showUiBackdrop', {
			type: 'url',
			url: '/desktop/templates/photo-gallery.html',
			param: {
				gallery_id: _id
			}
		});
	};
	$rootScope.showToolbar = true;
	/*********** SEARCH MODULE ******STARTS******/
	$scope.search = {};
	$scope.search.isOn = false;
	$scope.search.isSearching = false;
	$scope.search.isEmptyResult = false;
	$scope.search.keyword = "";
	$scope.search.results = [];
	var _req;
	$scope.search.onChange = function () {
		if (_req) $timeout.cancel(_req);
		if ($scope.search.keyword.trim().length > 0) {
			$scope.search.isOn = true;
			_req = $timeout(function () {
				$scope.search.isSearching = true;
				APIService.search($scope.search.keyword, function (_data) {
					$scope.search.isSearching = false;
					if (_data.isEmpty) {
						$scope.search.isEmptyResult = true;
					} else {
						$scope.search.isEmptyResult = false;
						$scope.search.results = _data;
						// clevertap.event.push("Desktop User Search", {
						// 	"Search Keyword": $scope.search.keyword.trim()
						// });
					}
					$window.scrollTo(0, 0);
				});
			}, 800);
		} else {
			$scope.search.isOn = false;
		}
		$rootScope.$on("closeSearch", function () {

			$scope.search.isOn = false;
			$scope.search.keyword = "";
		});
		$rootScope.$on("search:article", function (_obj, _param) {

			$scope.search.keyword = _param.keyword;
		})
		$scope.search.openArticle = function (section) {
			$scope.search.isOn = false;
			var _title = section.slug; //$filter('slug')(section.title);
			$state.go('article', {
				category: section.mainSection.parentSlug,
				subcategory: section.mainSection.slug,
				title: _title,
				id: section._id
			});
		}
	}
	$scope.$watch('search.keyword', function () {
		$scope.search.onChange();
	});
	/*********** SEARCH MODULE ******ENDS******/
	/*********** UI MODULE ******STARTS******/
	$rootScope.ui = {};
	/** LOADER UI **/
	$rootScope.ui.loader = {}
	$rootScope.ui.loader.isVisible = false;
	$rootScope.ui.loader.show = function () {
		$rootScope.ui.loader.isVisible = true;
		$rootScope.$broadcast('onUiLoaderVisible');
	};
	$rootScope.ui.loader.hide = function () {
		$rootScope.ui.loader.isVisible = false;
		$rootScope.$broadcast('onUiLoaderClose');
	};
	$rootScope.$on('showUiLoader', $rootScope.ui.loader.show);
	$rootScope.$on('hideUiLoader', $rootScope.ui.loader.hide);
	$rootScope.$on('$stateChangeStart', function () {

		$rootScope.ui.loader.show();
		$rootScope.ui.backdrop.hide(true);
		$rootScope.$broadcast('closeSearch');
	});
	$rootScope.$on('$viewContentLoaded', function () {

		$window.scrollTo(0, 0);
		$rootScope.ui.loader.hide();
	});
	$rootScope.openVideo = function (data) {
		$scope.$emit('showUiBackdrop', {
			type: "url",
			url: "/desktop/templates/video.html",
			param: data
		});
	}
	/** BACKDROP UI **/
	$rootScope.ui.backdrop = {};
	$rootScope.ui.backdrop.visible = false;
	$rootScope.ui.backdrop._url = "";
	$rootScope.ui.backdrop._html = "";
	$rootScope.ui.backdrop.data = {};
	$rootScope.ui.backdrop.load = function (_d) {

		$rootScope.ui.backdrop.visible = true;
		$rootScope.ui.backdrop._url = _d.url;
		$rootScope.ui.backdrop.data = _d.param;
	};
	$rootScope.ui.backdrop.loadHTML = function (_d) {
		$rootScope.ui.backdrop.visible = true;
		$rootScope.ui.backdrop._html = $sce.trustAsHtml(_d.data);
		$rootScope.ui.backdrop.data = _d.param;
	};
	$rootScope.ui.backdrop.show = function () {
		$rootScope.ui.backdrop.visible = true;
	};
	$rootScope.ui.backdrop.hide = function (clear) {

		if (clear) {
			$rootScope.ui.backdrop._html = "";
			$rootScope.ui.backdrop._url = "";
		}
		$rootScope.ui.backdrop.visible = false;
	};
	$rootScope.$on('hideUiBackdrop', $rootScope.ui.backdrop.hide);
	$rootScope.$on('showUiBackdrop', function (_obj, _param) {
		if (_param.type == 'url') {
			$rootScope.ui.backdrop.load(_param);
		} else if (_param.type == 'html') {
			$rootScope.ui.backdrop.loadHTML(_param);
		}
	});
	/** UI MESSAGE BOX **********/
	$rootScope.ui.messagebox = {};
	$rootScope.ui.messagebox.show = false;
	$rootScope.ui.messagebox.title = "";
	$rootScope.ui.messagebox.message = "";
	$rootScope.ui.messagebox.clear = function () {
		$rootScope.ui.messagebox.show = false;
		$rootScope.ui.messagebox.title = "";
		$rootScope.ui.messagebox.message = "";
	}
	$rootScope.$on('closeMessagebox', $rootScope.ui.messagebox.clear);
	$rootScope.$on('showMessagebox', function (_obj, _param) {
		$rootScope.ui.messagebox.show = true;
		$rootScope.ui.messagebox.title = (_param.title) ? _param.title : "";
		$rootScope.ui.messagebox.message = (_param.message) ? _param.message : "";
		$timeout($rootScope.ui.messagebox.clear, 3000);
	});
	/** UI MESSAGE BOX ENDS**********/
	$scope.ui.active = {};
	$scope.ui.active.category = '';
	$scope.ui.active.subcategory = '';
	$scope.$on('activateMenu', function (_d, param) {

		$scope.ui.active.category = param.category;
		$scope.ui.active.subcategory = param.subcategory;
	});
	$rootScope.Share = {};
	$rootScope.Share.fb = function (url) {
		FB.ui({
			method: 'share',
			display: 'popup',
			href: url,
		}, function (response) {});
	};
	$rootScope.openUserProfile = function (userId) {
		$scope.$emit('showUiBackdrop', {
			type: 'url',
			url: '/desktop/templates/user_profile.html',
			param: {
				otherUserId: userId
			}
		});
	}
	/************ META UPDATE ********************/
	$rootScope.$on('metaUpdate', function (_i, _param) {

		$rootScope.meta = _param;
	});
}]);
app.controller('backdropCtrl', ['$rootScope', '$scope', function ($rootScope, $scope) {
	$scope.backdrop = {};
	$scope.backdrop.hide = function (clear) {};
}]);
app.controller('videoCtrl', ['$scope', 'APIService', '$rootScope', function ($scope, APIService, $rootScope) {
	$scope.video = $rootScope.ui.backdrop.data

	var getVideo = function () {
		var _p_ = {
			article_id: $scope.video._id,
			userId: 0
		};
		if (user_info.islogged) {
			_p_.userId = user_info.userId;
		}
		APIService.getArticle(_p_, function (_d) {

			$scope.video = _d;
		});
	}
	getVideo();
	$scope.play = function (video) {
		$scope.video = video;
		getVideo();
	}
	// $rootScope.data.body = $sce.trustAsHtml(_d.data);
}]);
app.controller('likesCtrl', ['$scope', 'APIService', '$rootScope', function ($scope, APIService, $rootScope) {
	$scope.likes = [];
	$scope.article = $rootScope.ui.backdrop.data
	APIService.getLikes($scope.article.id, function (_d) {

		$scope.likes = _d;
	});
	$scope.currentUser = (user_info.islogged) ? user_info.userId : 0;
	$scope.setFollow = function (user, index) {

		if (user_info.islogged) {
			var _p = {};
			if (user_info.islogged) {
				_p.userId = user_info.userId;
			}
			_p.otherUserId = user;
			APIService.setFollow(_p, function (_r) {
				if (_r.success) {
					$scope.likes[index].follow = _r.following;
				}
			})
		} else {
			$scope.$emit('showMessagebox', {
				title: 'LOGIN',
				message: 'You need to login before continue'
			});
		}
	}
	// $rootScope.data.body = $sce.trustAsHtml(_d.data);
}]);
app.controller('photoGalleryCtrl', ['$scope', 'APIService', '$rootScope', function ($scope, APIService, $rootScope) {
	$scope.param = $rootScope.ui.backdrop.data;
	$scope.gallery = [];
	$scope.currentIndex = ($scope.param.goto) ? $scope.param.goto : 0;
	$scope.slickConfig = {
		enabled: true,
		autoplay: true,
		draggable: true,
		autoplaySpeed: 3000,
		method: {},
		event: {
			afterChange: function (event, slick, currentSlide, nextSlide) {

				$scope.currentIndex = currentSlide;
			},
			init: function (event, slick) {
				slick.slickGoTo($scope.currentIndex); // slide to correct index when init
			}
		}
	};
	if ($scope.param) {
		APIService.getPhotoGallery($scope.param.gallery_id, function (data) {
			$scope.gallery = data;
		});
	}
}]);
app.controller('loginCtrl', ['$scope', 'APIService', 'Storage', '$state', '$location', function ($scope, APIService, Storage, $state, $location) {
	$scope.display = {
		container: "login",
		txt: {
			login: "",
			signup: "",
			otp: "",
			reset: ""
		}
	}; //show login by default
	$scope.sign = { in: {},
		up: {}
	};
	$scope.validateEmail = function (_e) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(_e);
	}
	$scope.sign.up.submit = function () {
		var _params = {
			name: $scope.sign.up.name,
			email: $scope.sign.up.email,
			password: $scope.sign.up.password,
			phone: $scope.sign.up.phone
		}
		if (!$scope.sign.up.name || $scope.sign.up.name == "") {
			$scope.display.txt.signup = "PLEASE ENTER YOUR FULLNAME";
		} else if (!$scope.sign.up.phone || !(/^(\+91-|\+91|0)?\d{10}$/).test($scope.sign.up.phone)) {
			$scope.display.txt.signup = "PLEASE ENTER VALID MOBILE NUMBER";
		} else if (!$scope.validateEmail($scope.sign.up.email)) {
			$scope.display.txt.signup = "EMAIL ID IS INVALID";
		} else if (!$scope.sign.up.password || $scope.sign.up.password == "") {
			$scope.display.txt.signup = "PLEASE ENTER YOUR PASSWORD";
		} else {
			APIService.signup(_params, function (res) {
				if (res.success) {
					$scope.display.container = "otp";
				} else {
					$scope.display.txt.signup = res.message;
				}
			});
		}
	}
	$scope.verify = {};
	$scope.verify.code = "";
	$scope.verify.submit = function () {
		var _params = {
			verificationCode: $scope.verify.code
		}
		$scope.display.txt.otp = "Verifying..";
		APIService.verifyOTP(_params, function (res) {
			if (res.success) {
				if (res.success) {
					user_info = Storage.getCurrentUser();
					var _qry = $location.search();
					if (_qry.redirect) {

						$location.path(_qry.redirect);
					} else {

						$state.reload();
						$scope.$emit('hideUiBackdrop');
					}
				} else {
					$scope.display.txt.otp = res.message;
				}
			} else {
				$scope.display.txt.otp = "Unable to verify OTP";
			}
		});
	}
	$scope.verify.resend = function () {
		var _params = {
			email: ($scope.sign.up.email && $scope.sign.up.email != "") ? $scope.sign.up.email : $scope.sign.in.email
		}
		$scope.display.txt.otp = "Please wait..";
		APIService.resendOTP(_params, function (res) {
			if (res.success) {
				if (res.success) {
					$scope.display.txt.otp = "OTP Resent successfully";
				} else {
					$scope.display.txt.otp = res.message;
				}
			} else {
				$scope.display.txt.otp = "Unable to verify OTP";
			}
		});
	}
	/** RESET PSWD***/
	$scope.reset = {};
	$scope.reset.email = "";
	$scope.reset.submit = function () {
		var _params = {
			email: $scope.reset.email
		}
		if (!$scope.validateEmail($scope.reset.email)) {
			$scope.display.txt.reset = "Invalid email id";
		} else {
			$scope.display.txt.reset = "Please wait..";
			APIService.resetPassword(_params, function (res) {
				if (res.success) {
					if (res.success) {
						$scope.display.txt.reset = "New password has been sent to your email id.";
					} else {
						$scope.display.txt.reset = res.message;
					}
				} else {
					$scope.display.txt.reset = res.message;
				}
			});
		}
	}
	$scope.sign.in.submit = function () {
		var _params = {
			email: $scope.sign.in.email,
			password: $scope.sign.in.password
		}
		if (!$scope.validateEmail($scope.sign.in.email)) {
			$scope.display.txt.login = "EMAIL ID IS INVALID";
		} else if (!$scope.sign.in.password || $scope.sign.in.password == "") {
			$scope.display.txt.login = "PLEASE ENTER YOUR PASSWORD";
		} else {
			$scope.display.txt.login = "Logging in..";
			APIService.doLogin(_params, function (res) {
				if (res.success) {
					$scope.$emit('hideUiBackdrop');
					user_info = Storage.getCurrentUser();
					var _qry = $location.search();
					if (_qry.redirect) {

						$location.path("/" + _qry.redirect);
					} else {

						$location.path("/");
					}
				} else {
					if (res.isVerified) {
						$scope.display.txt.login = res.message;
					} else {
						$scope.display.container = "otp";
					}
				}
			});
		}
	}
	/************ SOCIAL LOGIN ***********/
	$scope.$on('event:social-sign-in-success', function (event, userDetails) {

		APIService.doFacebookLogin({
			access_token: userDetails.token
		}, function (res) {
			if (res.success) {
				$scope.$emit('hideUiBackdrop');
				user_info = Storage.getCurrentUser();
				var _qry = $location.search();

				if (_qry.redirect) {

					$location.path("/" + _qry.redirect);
				} else {

					$location.path("/");
				}
			} else {
				$scope.display.txt.login = res.message;
			}
		});
	});
}]);
app.controller('homeCtrl', ['$rootScope', '$scope', 'APIService', '$stateParams',  function ($rootScope, $scope, APIService, $stateParams) {
// 	var x2js = new X2JS();
// $scope.upcomingMovies=[];
// 	APIService.getUpcomingMovies(function(_xml) {
// 		if(_xml!=null){
// 			$scope.upcomingMovies=x2js.xml_str2json(_xml);
// 			console.log($scope.upcomingMovies);
// 		}
		
// 	});
	//$window.scrollTo(0, 100);
	/** REFRESH META **/
	var _meta = $stateParams.meta;
	_meta.thumbnail = "";
	_meta.pageURL = "http://www.spotboye.com";
	_meta.amp = false;
	$scope.$emit('metaUpdate', _meta);
	$scope.lists = [];
	$scope.$emit('showUiLoader');
	$scope.trendingSection=[];
	APIService.getDashboard(function (_lists) {
		$scope.lists = _lists;
		$scope.$emit('hideUiLoader');
	});
	$scope.loadedTrending=false;
	$scope.videosTrending=[];
		$scope.latestNewsTrending=[];
		$scope.photosTrending=[];
		$scope.quickiesTrending=[];
		$scope.celebTrending=[];
	APIService.getSidebar('dashboard', function (_d) {
		$scope.trendingSection = _d.trending;
		$scope.videosTrending=_d.trending.splice(0,3);
		$scope.latestNewsTrending=_d.trending.splice(0,3);
		$scope.photosTrending=_d.trending.splice(0,3);
		$scope.quickiesTrending=_d.trending.splice(0,3);
		$scope.celebTrending=_d.trending.splice(0,3);
		$scope.loadedTrending=true;
	});
	
	$scope._r = 1;
	$scope._random = function () {
		$scope._r = Math.floor(Math.random() * 100) + 1;
	}
	$scope.video_currentIndex = 0;
	$scope.videoSlickConfig = {
		enabled: true,
		autoplay: false,
		draggable: true,
		method: {
			slickGoTo: function (_i) {}
		},
		event: {
			afterChange: function (event, slick, currentSlide, nextSlide) {
				$scope.video_currentIndex = currentSlide;
			}
		}
	};
	$(window).scroll(function() {
if($('.billboardScroll').offset()){
	if ($('.navbar-inverse').offset().top > $('.billboardScroll').offset().top) {
		$(".billboard_ad").css("opacity","0"); }
	else {
		$(".billboard_ad").css("opacity","1");
	}
}

});
	$scope.gototop = function () {

		window.scrollTo(0, 0);
	}
	$scope.photo_currentIndex = 0;
	$scope.photoSlickConfig = {
		enabled: true,
		autoplay: true,
		draggable: true,
		autoplaySpeed: 3000,
		method: {
			slickGoTo: function (_i) {}
		},
		event: {
			afterChange: function (event, slick, currentSlide, nextSlide) {
				$scope.photo_currentIndex = currentSlide;
			}
		}
	}
}]);
// app.controller('beatboxCtrl', function ($scope) {


// });
app.controller('articleCtrl', ['$scope', '$rootScope', 'APIService', '$stateParams', '$sce', '$window', '$location', '$filter', '$http', '$timeout', '$compile', function ($scope, $rootScope, APIService, $stateParams, $sce, $window, $location, $filter, $http, $timeout, $compile) {
	var last_article = {
		id: 0,
		catId: 0
	};

	$scope.celebSlickConfig = {
		enabled: true,
		autoplay: true,
		draggable: true,
		method: {
			slickGoTo: function (_i) {}
		},
		event: {
			init: function (event, slick) {
				_s = slick
			}
		}
	};
	$scope._random = function () {
		$scope._r = Math.floor(Math.random() * 100) + 1;
	};
	$scope.article = {};
	$scope.more = {}
	$scope.currentUrl = removeURLParameter($location.absUrl());
	$scope.more.articles = [];
	var _requested = false;

	$scope.more.displat_txt = "";
	$scope.ui = {};
	$scope.ui.isLoaded = false;
	$scope.ui.like = function (_unlike) {
		if (user_info.islogged) {
			var _param = {
				articleId: $scope.article._id,
				userId: user_info.userId
			}
			APIService.likeArticle(_param, function (_d) {
				$scope.article.userLike = !_unlike;
				$scope.article.likesCount = (_unlike) ? ($scope.article.likesCount - 1) : ($scope.article.likesCount + 1);
			});
		} else {
			$scope.$emit('showUiBackdrop', {
				type: 'url',
				url: '/desktop/templates/login.html'
			})
		}
	}

	$scope.ui.openLikes = function () {
		$scope.$emit('showUiBackdrop', {
			type: 'url',
			url: '/desktop/templates/likes.html',
			param: {
				id: $scope.article._id
			}
		})
	}
	$scope.gototop = function () {

		window.scrollTo(0, 0);
	}
	$scope.parentCategory = $stateParams.category;
	$scope.subCategory = $stateParams.subcategory;
	$scope.$emit('activateMenu', {
		category: $stateParams.category,
		subcategory: $stateParams.subcategory
	});
	
	$scope.accent_color = "";

	var _p_ = {
		article_id: $stateParams.id,
		userId: 0
	};
	last_article.id = $stateParams.id;
	if (user_info.islogged) {
		_p_.userId = user_info.userId;
	}
	$scope.showYoutube = false;
	
	$(window).scroll(function() {
if($('.billboardScroll').offset()){
	if ($('.navbar-inverse').offset().top > $('.billboardScroll').offset().top-80) {
		$(".billboard_ad").css("opacity","0"); }
	else {
		$(".billboard_ad").css("opacity","1");
	}
}

});
	APIService.getArticle(_p_, function (_article) {

		$scope.$emit('hideUiLoader');
		if (!_article.success) {
			$scope.$emit('showMessagebox', {
				title: 'ERROR',
				message: 'Unable to retrieve article.'
			});
		} else {
			$scope.accent_color = _article.mainSection.color;
			if (_article.type == 'Video' && _article.youtubeId!="") {
				$scope.showYoutube = true;
				_article.youtubeId = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + _article.youtubeId);
			}
			if (_article.mainSection.parentSlug == "fashion") {
				$timeout(function () {
					var _elem = angular.element(".article-content");
					angular.forEach(_elem.find('img'), function (elem, _index) {
						if (_index > 0&&elem.src.indexOf('static.spotboye')>-1) {
							var _e = $compile(angular.element('<staqu-ad src="' + elem.src + '"></staqu-ad>'))($scope);
							angular.element(".article-content").find('img').eq(_index).after(_e);
						}
					});
				}, 500);
			}
			$scope.article = _article;
			$scope.article.tags_expand = false;
			$scope.article._tags = _article.tags;
			$scope.article.tags = (_article.tags && _article.tags != "") ? _article.tags.slice(0, 6) : "";
			$scope.article.body = $sce.trustAsHtml(_article.body);

			last_article.catId = _article.mainSection._id;
			$scope.ui.isLoaded = true;
			/** REFRESH META **/
			// clevertap.event.push("Desktop Article Page Loaded", {
			// 	//              "Article Link":window.location.href,
			// 	"Article Title": _article.title
			// });
			ga('send', {
				hitType: 'event',
				eventCategory: 'Article',
				eventAction: 'onLoad',
				//eventLabel: '/' + _article.mainSection.parentSlug + '/' + _article.mainSection.slug + '/' + ($filter('slug')(_article.title)) + '/' + _article._id
				eventLabel: '/' + _article.mainSection.parentSlug + '/' + _article.mainSection.slug + '/' + (_article.slug) + '/' + _article._id
			});

			$scope.$emit('metaUpdate', {
				title: _article.title,
				description: _article.shortBody,
				thumbnail: _article.thumbnail,
				pageURL: removeURLParameter(window.location.href),
			});

			var _breadcrumb = {};
			_breadcrumb["@context"] = "http://schema.org";
			_breadcrumb["@type"] = "BreadcrumbList";
			_breadcrumb.itemListElement = [{
				"@type": "ListItem",
				"position": 1,
				"item": {
					"@id": "https://www.spotboye.com/" + $scope.article.mainSection.parentSlug,
					"name": $scope.article.mainSection.parentSection
				}
			}, {
				"@type": "ListItem",
				"position": 2,
				"item": {
					"@id": "https://www.spotboye.com/" + $scope.article.mainSection.parentSlug + "/" + $scope.article.mainSection.slug,
					"name": $scope.article.mainSection.title
				}
			}];

			$('head').append("<script type='application/ld+json'>" + JSON.stringify(_breadcrumb) + "</script>");

			var _news_schema = {
				"@context": "http://schema.org",
				"@type": "NewsArticle",
				"headline": $scope.article.title,
				"image": {
					"@type": "ImageObject",
					"url": $scope.article.thumbnail,
					"height": 500,
					"width": 800
				},
				"datePublished": $scope.article.publishedAt_gmt,
				"author": {
					"@type": "Person",
					"name": $scope.article.authorName
				},
				"publisher": {
					"@type": "Organization",
					"name": "Spotboye",
					"logo": {
						"@type": "ImageObject",
						"url": "http://www.spotboye.com/images/logo.png",
						"width": 180,
						"height": 40
					}
				},
				"description": $scope.article.shortBody
			};

			$('head').append("<script type='application/ld+json'>" + JSON.stringify(_news_schema) + "</script>");

		}
	});




	$scope.expandTags = function () {
		$scope.article.tags_expand = true;
		$scope.article.tags = $scope.article._tags;
	}
	$scope.collapseTags = function () {
		$scope.article.tags_expand = false;
		$scope.article.tags = $scope.article._tags.slice(0, 6);
	}
	var _id = 1;
	$scope.getSlotID = function () {
		_id = _id + 1;
		return _id;
	}
	/*$scope.getNextArticle = function () {
		if (last_article.id != 0 && ($scope.article.type != 'Poll' && $scope.article.type != 'Contest')) {
			$scope.more.displat_txt = "Loading next article..";
			APIService.getNextArticle(last_article, function (data) {
				//_requested=false;
				$(window).bind('scroll', scrollBottomEvent);
				if (data._id) {
					$scope.more.displat_txt = "";
					last_article.id = data._id;
					last_article.catId = data.mainSection._id;
					data.body = $sce.trustAsHtml(data.body);
					$scope.more.articles.push(data);
					$window.ga('send', {
						hitType: 'pageview',
						page: '/' + data.mainSection.parentSlug + '/' + data.mainSection.slug + '/' + ($filter('slug')(data.title)) + '/' + data._id
					});


					ga('send', {
						hitType: 'event',
						eventCategory: 'NextArticle',
						eventAction: 'loaded',
						eventLabel: '/' + data.mainSection.parentSlug + '/' + data.mainSection.slug + '/' + ($filter('slug')(data.title)) + '/' + data._id
					});
				} else {
					$scope.more.displat_txt = "";
				}
			});
		} else {
			$scope.more.displat_txt = "Unable to retrieve more info";
		}
	}*/
	/**** POLL STARTS ********/
	var _answers = [];
	$scope.userpoll = {};
	$scope.display_txt = "";
	$scope.article.results = [];
	$scope.vote = function (question, option) {
		_answers.push({
			questionId: question,
			optionId: option
		});
		$scope.display_txt = "Voting! please wait..";
		var _poll_param = {
			answers: _answers,
			articleId: $scope.article._id
		};
		if (user_info.islogged) {
			_poll_param.userId = user_info.userId;
		}
		APIService.poll(_poll_param, function (_d) {
			if (_d.success) {
				$scope.article.results = _d.answerPercent;
				$scope.article.attempted = 1;
				$scope.display_txt = "";
			} else {
				$scope.display_txt = "Error occurred please try again";
			}
		})
	};
	/**** CONTEST STARTS ********/
	/*$scope.contest = {};
	var _s = {};
	$scope.slickConfig = {
		enabled: true
		, autoplay: false
		, draggable: false
		, method: {
			slickGoTo: function (_i) { }
		}
		, event: {
			init: function (event, slick) {
				_s = slick
			}
		}
	};
	var _answers = []
		, articleId = 0;
	$scope.usercontest = {};
	$scope.display_txt = "";
	$scope.article.results = 0;
	$scope.article.activeIndex = 0;
	$scope.usercontest.answer = function (qId, aId, nextIndex) {

		_answers.push({
			questionId: qId
			, optionId: aId
		});
		nextIndex = nextIndex + 1;
		if ($scope.article.questions.length > nextIndex) {
			_s.slickGoTo(nextIndex);
		}
		else {
			$scope.usercontest.vote()
		}
	};
	$scope.usercontest.vote = function () {
		$scope.display_txt = "Voting! please wait..";
		var _contest_param = {
			answers: _answers
			, articleId: $scope.article._id
		};
		if (user_info.islogged) {
			_contest_param.userId = user_info.userId;
		}
		APIService.poll(_contest_param, function (_d) {
			if (_d.success) {
				$scope.article.results = _d.points;
				$scope.article.attempted = 1;
				$scope.display_txt = "";
			}
			else {
				$scope.display_txt = "Error occurred please try again";
			}
		})
	};*/

	/*** CONTEST UI UPDATE **/

	$scope.contest = {};
	$scope.contest.selectedOption = -1;

	var _answers = [],
		articleId = 0;
	$scope.usercontest = {};
	$scope.display_txt = "";
	$scope.article.results = 0;
	$scope.contest.activeIndex = 0;
	$scope.contest.isDone = false;
	$scope.usercontest.answer = function () {

		_answers.push({
			questionId: $scope.article.questions[$scope.contest.activeIndex].questionId,
			optionId: $scope.contest.selectedOption
		});


		if ($scope.article.questions.length - 1 <= $scope.contest.activeIndex) {
			$scope.usercontest.vote();
		} else {
			$scope.contest.activeIndex = $scope.contest.activeIndex + 1;
			$scope.usercontest.question = $scope.article.questions[$scope.contest.activeIndex];
			$scope.contest.selectedOption = -1;
		}
	};
	$scope.usercontest.vote = function () {

		$scope.display_txt = "Submitting your Answer..";
		var _contest_param = {
			answers: _answers,
			articleId: $scope.article._id
		};
		if (user_info.islogged) {
			$scope.contest.isDone = true;

			_contest_param.userId = user_info.userId;

			APIService.poll(_contest_param, function (_d) {
				if (_d.success) {
					/* RESULTS OF THE CONTEST*/
					$scope.article.results = _d.points;
					$scope.article.attempted = 1;
					$scope.display_txt = "";
				} else {
					$scope.display_txt = "Error occurred please try again";
				}
			});
		} else {
			$scope.$emit('showUiBackdrop', {
				type: 'url',
				url: '/desktop/templates/login.html'
			});
		}
	};

}]);
app.directive('columbiaAd', function () {
	return {
		restrict: 'E',
		replace: true,
		link: function postLink(scope, elem, attrs) {
			var slotID = attrs.uid;//newValue;
			slotID=slotID+"_"+Math.round(Math.random()*200)*2;
			console.log("CLMBIA AD CALLED",slotID);
			setTimeout(() => {
				colombia.fns.push(function(){
					elem.html('<div id="clmbFixed" style="float:left;min-height:2px;width:100%;" data-slot="211038" data-position="1" data-section="0" data-ua="d" class="colombia"></div>');
						colombia.refresh("clmbFixed")
					 });		
				}, 300);
		}
	}
});
app.directive('googleAds', function () {
	return {
		restrict: 'E',
		replace: true,
		link: function postLink(scope, elem, attrs) {
			var slotID = attrs.id;//newValue;
                slotID=slotID+"_"+Math.round(Math.random()*60)*2;
                elem.html('<div id="' + slotID + '"></div>');
                var slot = {};
                setTimeout(function(){    
					googletag.cmd.push(function () {
						
						switch (attrs.type) {
							case 'HP_Insticator_300x350':
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye.com_HP_Insticator_300x350', [300.0,350.0],slotID);
							 break;
						
							case 'HP_728x90_MID1':
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_HP_728x90_MID1', [728.0,90.0],slotID);
							 break;
							case 'HP_728x90_MID2':
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_HP_728x90_MID2', [728.0,90.0],slotID);
							 break;
							case 'HP_728x90_BTF':
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_HP_728x90_BTF', [728.0,90.0],slotID);
							 break;
							 case 'HP_300x250_Right_ATF':
							// googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_Mid', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_HP_300x250_Right_ATF', [300,250],slotID);
							 break;
							 case 'HP_300x250_Right_MID':
							 //googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_HP_300x250_Right_MID', [300,250],slotID);
							 break;
							 case 'HP_300x250_Right_BTF':
							// googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_HP_300x250_Right_BTF', [300,250],slotID);
							 break;
							 case 'HP_300x600_Right_BTF':
							// googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_HP_300x600_Right_BTF', [300,600],slotID);
							 break;
							
							 case 'AP_970x250_Top':
							// googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_AP_970x250_Top', [970,250],slotID);
							 break;
							 case 'AP_300x250_Right_ATF':
							// googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_AP_300x250_Right_ATF', [300,250],slotID);
							 break;
							 case 'AP_300x250_Right_MID1':
							// googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_AP_300x250_Right_MID1', [300,250],slotID);
							 break;
							 case 'AP_300x250_Right_MID2':
							// googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_AP_300x250_Right_MID2', [300,250],slotID);
							 break;
							 case 'AP_728x90_ATF':
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_AP_728x90_ATF', [728.0,90.0],slotID);
							 break;
							 case 'AP_728x90_BTF':
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_AP_728x90_BTF', [728.0,90.0],slotID);
							 break;
							 case 'AP_728x90_BTF2':
							 googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Desktop_AP_728x90_BTF2', [728.0,90.0],slotID);
							 break;

							 case 'OOP_Mobile_Native':
							 googletag.defineOutOfPageSlot('/97172535/Spotboye.com_Mobile_Native', slotID)
							 .addService(googletag.pubads());
								 googletag.pubads();
								 googletag.enableServices();
								 googletag.display(slotID);
							 break;
						 
							 
							case 'Ad1_1_300X250':
								slot = googletag.defineSlot('/97172535/Test1_adunit_300x250', [300, 250], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad1_2_300X250':
								slot = googletag.defineSlot('/97172535/HP_RANDOMBANNER_300X250', [300, 250], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad1_3_300X250':
								slot = googletag.defineSlot('/97172535/HP_RANDOMBANNER1_300X250', [300, 250], slotID)
									.setTargeting("article", "infinitescroll").addService(googletag.pubads());
								break;

							case 'HP_Skyscrapper_160X600':
								slot = googletag.defineSlot('/97172535/Skyscrapper', [160, 600], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'impulse':
							
								slot = googletag.defineOutOfPageSlot('/97172535/Impulse_Pic', slotID+"_child").addService(googletag.pubads());
								break;

							case 'Ad2_1_LB_728X90':
								slot = googletag.defineSlot('/97172535/Test2_adunit_728X90', [728, 90], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad2_2_LB_728X90':
								slot = googletag.defineSlot('/97172535/SECTION1_728X90', [728, 90], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad2_3_LB_728X90':
								slot = googletag.defineSlot('/97172535/SECTION3_728X90', [728, 90], slotID)
									.setTargeting("article", "infinitescroll").addService(googletag.pubads());
								break;

							case 'Ad2_1_BTF_LB_728X90':
								slot = googletag.defineSlot('/97172535/SECTION_4_728X90', [728, 90], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad2_2_BTF_LB_728X90':
								slot = googletag.defineSlot('/97172535/SECTION_5_728X90', [728, 90], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad2_3_BTF_LB_728X90':
								slot = googletag.defineSlot('/97172535/SECTION_7_728X90', [728, 90], slotID).setTargeting("article", "infinitescroll").addService(googletag.pubads());
								break;

							case 'Interstitial_Adunit':
								
							
								slot = googletag.defineOutOfPageSlot('/97172535/Interstitial_Adunit',  slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'vibe':
								slot = googletag.defineSlot('/97172535/Vbe_In-article_1x1', [1, 1], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad1_1_BTF_300X250':
								slot = googletag.defineSlot('/97172535/MOBILE_ARICLE_1_320X250', [300, 250], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;

							case 'Ad2_3_MBTF_300X250':
								slot = googletag.defineSlot('/97172535/MOBILE_ARICLE_2_320X250', [300, 250], slotID).setTargeting('pagetype', ['test']).addService(googletag.pubads());
								break;


						}

						// googletag.pubads();
						// googletag.enableServices();

						// googletag.display(slotID);
						//googletag.refresh([slot]);
					});
				},300);
				// });
			 }
		 };
	 });
// app.directive('gad', function () {
//   return {
//      restrict: 'E',
//      replace: true,
//
//      template: '<ins class="adsbygoogle" style="display:inline-block;" data-ad-client="ca-pub-2223758409184752" data-ad-slot="9910965544" data-ad-format="autorelaxed"></ins>',
// controller: function () {
//                    (adsbygoogle = window.adsbygoogle || []).push({});
//                  }
//  };
//     });
app.directive('staquAd', function ($http) {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: '/' + WEBSERVICE.DEVICE + '/templates/staqu.html',
		scope: {
			src: '@'
		},
		link: function postLink(scope, elem, attrs) {
			scope.loaded = false;

			$http.get('https://videos.snapnbuy.in/api/v1/image-recom?image_url=' + scope.src).then(function (res) {
				scope.loaded = true;

				scope.data = res.data.results;

			}, function (err) {
				scope.loaded = true;
			});

		}
	}

});



app.controller('categoryCtrl', ['$rootScope', '$scope', 'APIService', '$stateParams', '$state', '$sce', '$filter', function ($rootScope, $scope, APIService, $stateParams, $state, $sce, $filter) {
	var page = 1;
	var limit = 21;
	$scope.isLoaded=false;
	$scope.category = {
		isParent: true,
		lists: [],
		maincategory: "",
		subcategory: ""
	};
	$scope._random = function () {
		$scope._r = Math.floor(Math.random() * 100) + 1;
	}
	$scope.submenu = [];
	$scope.paginateMore = function () {
		APIService.getBySubCategory({
			parent: $scope.category.maincategory,
			child: $scope.category.subcategory,
			page: page++,
			limit: limit
		}, function (_res) {
			$scope.category.color = _res.color;
			if (_res.data.length > 0 && page - 1 == 1) {
				$scope.category.justin = _res.data.splice(0, 6);
				$scope.category.lists = _res.data;
			} else {
				$scope.category.lists.push.apply($scope.category.lists, _res.data);

			}
			/** REFRESH META **/
			if (page == 0) {
	
				$scope.$emit('metaUpdate', {
					title: _res.metaTitle,
					description: _res.metaDescription,
					pageURL: removeURLParameter(window.location.href)
				});
			}
		});
	}


	if ($stateParams.category && $stateParams.category.trim().length > 0) {
		$scope.category.maincategory = $stateParams.category;
	}
	if ($stateParams.subcategory && $stateParams.subcategory.trim().length > 0) {
		$scope.category.subcategory = $stateParams.subcategory;
		$scope.category.isParent = false;
	}
	$scope.$emit('activateMenu', {
		category: $stateParams.category,
		subcategory: $stateParams.subcategory
	});
	$rootScope.$watch('menus', function () {
		angular.forEach($rootScope.menus, function (sub, i) {
			if (sub.slug == $scope.category.maincategory) {
				$scope.submenu = sub.subMenu;
			}
		});
	});
	$(window).scroll(function() {
if($('.billboardScroll').offset()){
	if ($('.navbar-inverse').offset().top > $('.billboardScroll').offset().top) {
		$(".billboard_ad").css("opacity","0"); }
		else {
		$(".billboard_ad").css("opacity","1");
		}
}

	});
	$scope.gototop = function () {

		window.scrollTo(0, 0);
	}
	if ($scope.category.isParent) {
		APIService.getByCategory($stateParams.category, function (_res) {
$scope.isLoaded=true;
			if (_res.success) {
				$scope.category.color = _res.color;
				$scope.category.lists = _res.data;
				/** REFRESH META **/
				// clevertap.event.push("Desktop Primary Menu Clicked", {
				// 	"Category": _res.category
				// });

				$scope.$emit('metaUpdate', {
					title: _res.metaTitle,
					description: _res.metaDescription,
					pageURL: removeURLParameter(window.location.href)
				});
			} else {
				$state.go('not-found');
			}
		});
	} else {
		$scope.paginateMore(); //first request
	}
	$scope.openPost = function (post, index) {
		var _post = post; //unneccessary variable to override type
		if (_post.media.length > 0 && _post.type == 'Article') {
			_post.type = 'Video';
		}
		if (_post.media.length == 0 && _post.type == "Video") {
			_post.type = 'Article';
		}
		switch (_post.type) {
			case 'Video':
				if (index) {
					post.prev = {};
					post.prev = ($scope.category.lists[index - 1]) ? $scope.category.lists[index - 1] : {};
				}
				$rootScope.openVideo(post);
				break;
			case 'Poll':
				$scope.$emit('showUiBackdrop', {
					type: 'url',
					url: '/desktop/templates/poll.html',
					param: {
						id: post._id
					}
				})
				break;
			case 'Contest':
				$scope.$emit('showUiBackdrop', {
					type: 'url',
					url: '/desktop/templates/contest.html',
					param: {
						id: post._id
					}
				})
				break;
			default:
				var _title =post.slug;// $filter('slug')(post.title);
				$state.go('article', {
					category: post.mainSection.parentSlug,
					subcategory: post.mainSection.slug,
					title: _title,
					id: post._id
				});
		}
	}
	$scope.photo_currentIndex = 0;
	$scope.photoSlickConfig = {
		enabled: true,
		autoplay: true,
		draggable: true,
		autoplaySpeed: 3000,
		method: {
			slickGoTo: function (_i) {}
		},
		event: {
			afterChange: function (event, slick, currentSlide, nextSlide) {
				$scope.photo_currentIndex = currentSlide;
			}
		}
	};


}]);
app.controller('celebrityCtrl', ['$scope', 'APIService', '$stateParams', function ($scope, APIService, $stateParams) {
	$scope._random = function () {
		$scope._r = Math.floor(Math.random() * 100) + 1;
	}
	$scope.profile = {};
	$scope.profile.follow = false;
	if ($stateParams.id) {
		var _p = {}
		if (user_info.islogged) {
			_p.userId = user_info.userId;
		}
		_p.celebrityId = $stateParams.id;
		APIService.getCelebrity(_p, function (data) {
			$scope.profile = data;

			// clevertap.event.push("Desktop celebrity Viewed", {
			// 	"Celeb name": data.name
			// });
			/** REFRESH META **/
			$scope.$emit('metaUpdate', {
				title: data.name,
				description: data.name,
				pageURL: removeURLParameter(window.location.href)
			});
		});
	}
	$scope.setFollow = function (follow) {
		if (user_info.islogged) {
			APIService.setCelebrityFollow(_p, function (_r) {
				if (_r.success) {
					$scope.profile.follow = _r.following;
				}
			})
		} else {
			$scope.$emit('showUiBackdrop', {
				type: 'url',
				url: '/desktop/templates/login.html'
			})
		}
	}
}]);
app.controller('allCelebrityCtrl', ['$scope', 'APIService', '$stateParams', '$timeout', function ($scope, APIService, $stateParams, $timeout) {
	$scope._random = function () {
		$scope._r = Math.floor(Math.random() * 100) + 1;
	}
	$scope.list = {};
	$scope.alphabets = {};
	$scope.alphabets.list = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
	$scope.alphabets.activeIndex = 0;
	APIService.getCelebrities(function (data) {
		$scope.list = data;
	});
	var _req;
	$scope.fetchCelebs = function (keyword, delay) {
		var request = function () {
			APIService.findCelebs({
				name: keyword
			}, function (d) {
				if (d.success) {
					$scope.list.celebs = d.celebs;
				} else {}
			});
		}
		if (delay && keyword != "") {

			if (_req) {
				$timeout.cancel(_req);
			}
			_req = $timeout(function () {
				request();
			}, 800);
		} else {
			request();
		}
	}
	jQuery(document).scroll(function () {

if($('.billboardScroll').offset()){
	if ($('.navbar-inverse').offset().top > $('.billboardScroll').offset().top) {
	    $(".billboard_ad").css("opacity","0"); }
	else {
	    $(".billboard_ad").css("opacity","1");
	}
}

	});
	$scope.setFollow = function (index, isTrending) {
		if (user_info.islogged) {
			var _p = {};
			_p.celebrityId = $scope.list[isTrending][index]._id;
			_p.userId = user_info.userId;
			APIService.setCelebrityFollow(_p, function (_r) {
				if (_r.success) {
					$scope.list[isTrending][index].follow = _r.following;
				}
			})
		} else {
			$scope.$emit('showUiBackdrop', {
				type: 'url',
				url: '/desktop/templates/login.html'
			})
		}
	}
}]);
app.controller('pollCtrl', ['$scope', 'APIService', '$rootScope', function ($scope, APIService, $rootScope) {
	$scope.param = $rootScope.ui.backdrop.data;
	$scope.poll = {};
	var _answers = [],
		articleId = 0;
	$scope.userpoll = {};
	$scope.display_txt = "";
	$scope.userpoll.results = [];
	$scope.userpoll.nextPoll = function (item) {
		var _article_param = {
			article_id: item._id
		};
		if (user_info.islogged) {
			_article_param.userId = user_info.userId;
		}
		APIService.getArticle(_article_param, function (data) {
			$scope.poll = data;
		});
	}
	$scope.userpoll.vote = function (question, option) {
		_answers.push({
			questionId: question,
			optionId: option
		});
		$scope.display_txt = "Voting! please wait..";
		var _poll_param = {
			answers: _answers,
			articleId: articleId
		};
		if (user_info.islogged) {
			_poll_param.userId = user_info.userId;
		}
		APIService.poll(_poll_param, function (_d) {
			if (_d.success) {
				$scope.userpoll.results = _d.answerPercent;
				$scope.poll.attempted = 1;
				$scope.display_txt = "";
			} else {
				$scope.display_txt = "Error occurred please try again";
			}
		})
	};
	if ($scope.param) {
		articleId = $scope.param.id;
		var _article_param = {
			article_id: articleId
		};
		if (user_info.islogged) {
			_article_param.userId = user_info.userId;
		}
		APIService.getArticle(_article_param, function (data) {
			$scope.poll = data;
		});
	}
}]);
app.controller('contestCtrl', ['$scope', 'APIService', '$rootScope', function ($scope, APIService, $rootScope) {
	$scope.param = $rootScope.ui.backdrop.data;
	$scope.contest = {};
	var _slick = {};
	$scope.slickConfig = {
		enabled: true,
		autoplay: false,
		draggable: false,
		method: {},
		event: {
			init: function (event, slick) {
				_slick = slick
			}
		}
	};
	var _answers = [],
		articleId = 0;
	$scope.usercontest = {};
	$scope.display_txt = "";
	$scope.usercontest.results = 0;
	$scope.usercontest.activeIndex = 0;
	$scope.usercontest.answer = function (qId, aId, nextIndex) {
		if (user_info.islogged) {


			_answers.push({
				questionId: qId,
				optionId: aId
			});
			nextIndex = nextIndex + 1;
			if ($scope.contest.questions.length > nextIndex) {
				_slick.slickGoTo(nextIndex);
			} else {
				$scope.usercontest.vote();
			}
		} else {
			$scope.$emit('showUiBackdrop', {
				type: 'url',
				url: '/desktop/templates/login.html'
			});
		}
	};
	$scope.usercontest.vote = function () {
		$scope.display_txt = "Checking score! Please wait..";
		var _contest_param = {
			answers: _answers,
			articleId: articleId
		};
		if (user_info.islogged) {
			_contest_param.userId = user_info.userId;
		}
		APIService.poll(_contest_param, function (_d) {
			if (_d.success) {
				$scope.usercontest.results = _d.points;
				$scope.contest.attempted = 1;
				$scope.display_txt = "";
			} else {
				$scope.display_txt = "Error occurred please try again";
			}
		})
	};
	if ($scope.param) {
		articleId = $scope.param.id;
		var _article_param = {
			article_id: articleId
		};
		if (user_info.islogged) {
			_article_param.userId = user_info.userId;
		}
		APIService.getArticle(_article_param, function (data) {
			$scope.contest = data;
		});
	}
}]);
app.controller('searchCtrl', ['$scope', '$stateParams', function ($scope, $stateParams) {
	if ($stateParams.q) {
		$scope.$emit("search:article", {
			keyword: $stateParams.q
		});
	}
}]);
app.controller('profileCtrl', ['$rootScope', '$scope', 'APIService', 'Storage', function ($rootScope, $scope, APIService, Storage) {
	var _p = Storage.getCurrentUser();
	if (_p && _p.userId) {
		$scope.profile = _p;
		$scope.liked = [];
		$scope.followers = [];
		APIService.getMyProfile({
			userId: _p.userId
		}, function (_d) {
			Storage.saveUserDetails("profilePic", _d.userImage);
			Storage.saveUserDetails("name", _d.userName);
			$scope.profile.profilePic = _d.userImage;
			$scope.liked = _d.likedArticles;
			$scope.followers = _d.follower;
			$scope.following = _d.following;
		});
	} else {
		$scope.$emit('hideUiBackdrop');
	}
}]);
app.controller('userProfileCtrl', ['$rootScope', '$scope', 'APIService', function ($rootScope, $scope, APIService) {
	var _p = {};
	_p = $rootScope.ui.backdrop.data;
	if (user_info.islogged) {
		_p.userId = user_info.userId;
	}
	$scope.showFollow = false;
	$scope.profile = {};
	if (_p.otherUserId) {
		$scope.liked = [];
		$scope.followers = [];
		APIService.getUserProfile(_p, function (_d) {
			if (_d.success) {
				$scope.profile = _d;
				if (_p.userId) {
					$scope.showFollow = (_p.userId == _p.otherUserId) ? false : true;
				}
			} else {
				$scope.$emit('showMessagebox', {
					title: 'NO USER FOUND',
					message: "This user doesn't exist"
				});

			}
		});
		$scope.setFollow = function (follow) {
			if (user_info.islogged) {
				APIService.setFollow(_p, function (_r) {
					if (_r.success) {
						$scope.profile.follow = _r.following;
					}
				})
			} else {
				$scope.$emit('showMessagebox', {
					title: 'LOGIN',
					message: 'You need to login before continue'
				});
			}
		}
	} else {
		$scope.$emit('hideUiBackdrop');
	}
}])
/*********** DIRECTIVES *******************/
app.directive('avatars', function () {
	return {
		restrict: 'E',
		templateUrl: '/' + WEBSERVICE.DEVICE + '/templates/avatars.html',
		scope: {
			content: "="
		},
		controller: 'avatarsPartial'
	}
});
app.directive('sidebar', function () {
	return {
		restrict: 'E',
		templateUrl: '/' + WEBSERVICE.DEVICE + '/templates/sidebar.html',
		scope: {
			for: "@"
		},
		controller: 'sidebarPartial'
	}
});
app.directive('fbComments', function () {
	function createHTML(href, numposts, colorscheme) {
		return '<div class="fb-comments" ' + 'data-href="' + href + '" ' + 'data-numposts="' + numposts + '" ' + 'data-colorsheme="' + colorscheme + '">' + '</div>';
	}
	return {
		restrict: 'E',
		replace: true,
		link: function postLink(scope, elem, attrs) {
			attrs.$observe('href', function (newValue) {
				var href = newValue;
				var numposts = attrs.numposts || 5;
				var colorscheme = attrs.colorscheme || 'light';
				elem.html(createHTML(href, numposts, colorscheme));
				//if(typeof FB !== 'undefined'){
				FB.XFBML.parse(elem[0]);
				//`}
			});
		}
	};
});
app.directive('sticky', function () {
	return {
		restrict: 'A',
		replace: true,
		link: function postLink(scope, elem, attrs) {
			elem.stick_in_parent();
		}
	};
});
app.directive('forkads', function () {
	return {
		restrict: 'E',
		replace: true,
		link: function postLink(scope, elem, attrs) {
			elem.html('<script id="_forkIAVScript" type="text/javascript" src="//s3-ap-southeast-1.amazonaws.com/vibecreatives/Inarticle/iav.js?publisher=spotboye&cb=%%CACHEBUSTER%%&gclk=%%CLICK_URL_ESC%%" async><\/script>');
		}
	};
});
var _e;
var _rootScope;
var _scope;
app.directive("scroll", function ($window, $rootScope) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			scope.lastHit = 0;
			_e = element[0];
			_scope = scope;
			_rootScope = $rootScope;
			//            angular.element(
			$(window).bind("scroll", scrollBottomEvent);
		}
	}
});

function scrollBottomEvent(event) {
	if (this.pageYOffset >= (_e.offsetHeight - this.innerHeight)) {
		if ($(window).scrollTop() + $(window).height() > $(document).height() - 800) {

			_scope.lastHit = _e.offsetHeight - this.innerHeight;
			$(window).unbind('scroll', scrollBottomEvent);
			_rootScope.$broadcast('onScrollBottom');
		}
	}
}
app.controller('avatarsPartial', ['$scope', '$state', function ($scope, $state) {
	$scope.data = [];
	if ($scope.content instanceof Array) {
		//array
		$scope.data = $scope.content;
	} else {
		//add single object to an empty array
		$scope.data.push($scope.content);
	}
}]);
app.controller('sidebarPartial', ['$filter', '$scope', 'APIService', '$state', function ($filter, $scope, APIService, $state) {
	$scope._locale = (localStorage.getItem('locale') == 'hi') ? "/hi" : '';
	$scope.data = [];
	$scope.openArticle = function (section) {

		var _title =section.slug //$filter('slug')(section.title);
		$state.go('article', {
			category: section.mainSection.parentSlug,
			subcategory: section.mainSection.slug,
			title: _title,
			id: section._id
		});
	}
	switch ($scope.for) {
		case 'article':
			APIService.getSidebar('article', function (_d) {
				$scope.data = _d;
			});
			break;
		default:
			//dashboard
			APIService.getSidebar('dashboard', function (_d) {
				$scope.data = _d;
			});
			break;
	}
	/***********POLLS ******STARTS******/
	var _answers = [];
	$scope.polls = {};
	$scope.polls.vote = function (_ndex, question, option) {
		_answers.push({
			questionId: question,
			optionId: option
		});

		$scope.display_txt = "";
		var _poll_param = {
			answers: _answers,
			articleId: $scope.data.poll[0]._id
		};
		if (user_info.islogged) {
			_poll_param.userId = user_info.userId;
		}
		APIService.poll(_poll_param, function (_d) {

			if (_d.success) {
				$sc	// APIService.getSocialFeed('facebook', function (data) {
	// 	$scope.social.facebook = data;
	// });
	// APIService.getSocialFeed('twitter', function (data) {
	// 	$scope.social.twitter = data;
	// });ope.data.poll[0].answers = _d.answerPercent;
				$scope.data.poll[0].attempted = 1;
				$scope.display_txt = "";
			} else {
				$scope.display_txt = "Error occurred please try again";
			}
		})
	};
	/*********** SOCIAL FEEDS ******STARTS******/
	$scope.social = {};
	$scope.social.facebook = [];
	$scope.social.twitter = [];
	$scope.social.instagram = [];

	jQuery(document).ready(function ($) {
		var stickySidebar = $('.sidebar').offset().top;
		var checkLength = false;
		//  window.addEventListener("resize", playVideos, false);
		//  window.addEventListener("scroll", playVideos, false);
		$(window).scroll(function () {
			if ($('.article-container').length > 0) {}
		});
	});
}]);


jQuery(document).ready(function ($) {
	//  $("body").niceScroll();
	var sw = screen.width;
	var hsw = Math.floor(sw / 2);
	$('body').on('mouseenter', '.checkposition>li', function () {
		$('.checkposition>li').css('background-color', 'transparent');
		$(this).css('background-color', $(this).css('border-top-color'));
		if ($(this).offset().left > hsw) {
			$(this).find('ul').addClass('showinRight').removeClass('showinLeft');
		} else {
			$(this).find('ul').removeClass('showinRight').addClass('showinLeft');
		}
	});
	$('body').on('mouseleave', '.checkposition>li', function () {
		$(this).css('background-color', 'transparent');
	})
	var mainHeader = $('.cd-auto-hide-header'),
		secondaryNavigation = $('.cd-secondary-nav'), //this applies only if secondary nav is below intro section
		belowNavHeroContent = $('.sub-nav-hero'),
		headerHeight = mainHeader.height();
	//set scrolling variables
	var scrolling = false,
		previousTop = 0,
		currentTop = 0,
		scrollDelta = 10,
		scrollOffset = 150;
	mainHeader.on('click', '.nav-trigger', function (event) {
		// open primary navigation on mobile
		event.preventDefault();
		mainHeader.toggleClass('nav-open');
	});
	$(window).on('scroll', function () {
		if (!scrolling) {
			scrolling = true;
			(!window.requestAnimationFrame) ? setTimeout(autoHideHeader, 250): requestAnimationFrame(autoHideHeader);
		}
	});
	$(window).on('resize', function () {
		headerHeight = mainHeader.height();
	});

	function autoHideHeader() {
		var currentTop = $(window).scrollTop();
		(belowNavHeroContent.length > 0) ? checkStickyNavigation(currentTop) // secondary navigation below intro
			: checkSimpleNavigation(currentTop);
		previousTop = currentTop;
		scrolling = false;
	}

	function checkSimpleNavigation(currentTop) {
		//there's no secondary nav or secondary nav is below primary nav
		if (previousTop - currentTop > scrollDelta) {
			//if scrolling up...
			mainHeader.removeClass('is-hidden');
		} else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
			//if scrolling down...
			mainHeader.addClass('is-hidden');
		}
	}

	function checkStickyNavigation(currentTop) {
		//secondary nav below intro section - sticky secondary nav
		var secondaryNavOffsetTop = belowNavHeroContent.offset().top - secondaryNavigation.height() - mainHeader.height();
		if (previousTop >= currentTop) {
			//if scrolling up...
			if (currentTop < secondaryNavOffsetTop) {
				//secondary nav is not fixed
				mainHeader.removeClass('is-hidden');
				secondaryNavigation.removeClass('fixed slide-up');
				belowNavHeroContent.removeClass('secondary-nav-fixed');
			} else if (previousTop - currentTop > scrollDelta) {
				//secondary nav is fixed
				mainHeader.removeClass('is-hidden');
				secondaryNavigation.removeClass('slide-up').addClass('fixed');
				belowNavHeroContent.addClass('secondary-nav-fixed');
			}
		} else {
			//if scrolling down...
			if (currentTop > secondaryNavOffsetTop + scrollOffset) {
				//hide primary nav
				mainHeader.addClass('is-hidden');
				secondaryNavigation.addClass('fixed slide-up');
				belowNavHeroContent.addClass('secondary-nav-fixed');
			} else if (currentTop > secondaryNavOffsetTop) {
				//once the secondary nav is fixed, do not hide primary nav if you haven't scrolled more than scrollOffset
				mainHeader.removeClass('is-hidden');
				secondaryNavigation.addClass('fixed').removeClass('slide-up');
				belowNavHeroContent.addClass('secondary-nav-fixed');
			}
		}
	}
});
$.fn.isOnScreen = function () {
	var win = $(window);
	var viewport = {
		top: win.scrollTop(),
		left: win.scrollLeft()
	};
	viewport.right = viewport.left + win.width();
	viewport.bottom = viewport.top + win.height();
	var bounds = this.offset();
	if (bounds) {
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();
		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	}
};

function playVideos() {
	for (var i = 0; i < playersVideo.length; i++) {
		var videoPlayer = $('.slick-current').find('#' + playersVideo[i]);

		var videoPlayerElem = videojs('#' + playersVideo[i]);
		if (videoPlayer.isOnScreen()) {
			//videoPlayerElem.play();
			videoPlayerElem.ima.initializeAdDisplayContainer();
			videoPlayerElem.ima.requestAds();
			videoPlayerElem.play();
			//return false;
		} else {
			videoPlayerElem.pause();
		}
	}
}
