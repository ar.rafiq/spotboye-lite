app.config(["$stateProvider", "$locationProvider", "$urlRouterProvider", "socialProvider", "$urlMatcherFactoryProvider", function ($stateProvider, $locationProvider, $urlRouterProvider, socialProvider, $urlMatcherFactoryProvider) {
	socialProvider.setFbKey({
		appId: "630344967097986",
		apiVersion: "v2.6"
	});

	localStorage.setItem('locale', 'en');

	$locationProvider.html5Mode(true);
	$urlMatcherFactoryProvider.strictMode(false);
	$urlRouterProvider.otherwise("/not-found");
	$stateProvider.state('home', {
			url: '/',
			templateUrl: '/' + DEVICE_TYPE + '/views/dashboard.html',
			controller: 'homeCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		})
		// .state('beatbox', {
		// 	url: '/beatbox',
		// 	templateUrl: '/' + DEVICE_TYPE + '/views/beatbox.html',
		// 	controller: 'beatboxCtrl'
		// }).state('beatbox-anything', {
		// 	url: '/beatbox/:any',
		// 	templateUrl: '/' + DEVICE_TYPE + '/views/beatbox.html',
		// 	controller: 'beatboxCtrl'
		// })
		.state('salman-apology', {
			url: '/apology',
			templateUrl: '/' + DEVICE_TYPE + '/views/apology.html'
		}).state('twitter-feeds', {
			url: '/binge/twitter',
			templateUrl: '/' + DEVICE_TYPE + '/views/binge-twitter.html'
		})
		.state('celebrities', {
			url: '/celebrity',
			templateUrl: '/' + DEVICE_TYPE + '/views/celebrities.html',
			controller: 'allCelebrityCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('celebrity-name', {
			url: '/celebrity/:id',
			templateUrl: '/' + DEVICE_TYPE + '/views/celebrity.html',
			controller: 'celebrityCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('celebrity', {
			url: '/celebrity/:celebrity/:id',
			templateUrl: '/' + DEVICE_TYPE + '/views/celebrity.html',
			controller: 'celebrityCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('search', {
			url: '/search?q',
			templateUrl: '/' + DEVICE_TYPE + '/views/search.html',
			controller: 'searchCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('not-found', {
			url: '/not-found',
			template: "<h3 class='container'>NOT FOUND</h3>"
		}).state('terms', {
			url: '/terms',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/terms.html'
		}).state('sitemap', {
			url: '/sitemap',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/sitemap.html'
		}).state('disclaimer', {
			url: '/disclaimer',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/disclaimer.html'
		}).state('privacypolicy', {
			url: '/privacypolicy',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/privacypolicy.html'
		}).state('contactus', {
			url: '/contactus',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/contactus.html'
		}).state('aboutus', {
			url: '/aboutus',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/about.html'
		}).state('logout', {
			url: '/logout',
			controller: function (Storage, $window) {
				Storage.clearUser();
				user_info = {
					islogged: false,
					userId: 0
				}
				$window.location.href = "/";
			}
		}).state('parentCategory', {
			url: '/:category',
			templateUrl: '/' + DEVICE_TYPE + '/views/category.html',
			controller: 'categoryCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('childCategory', {
			url: '/:category/:subcategory',
			templateUrl: '/' + DEVICE_TYPE + '/views/category.html',
			controller: 'categoryCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
				}
			}
		}).state('article', {
			url: '/:category/:subcategory/:title/:id',
			templateUrl: '/' + DEVICE_TYPE + '/views/article.html',
			controller: 'articleCtrl',
			params: {
				meta: {
					title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
					description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
				}
			}
		});
}]);
app.run(['$rootScope', '$location', '$window', 'dfp', '$state', function ($rootScope, $location, $window, dfp, $state) {
	// initialise google analytics
	$window.ga('create', 'UA-53522784-1', 'auto');
	//	$window.ga('require', 'GTM-TMRTK62');

	$rootScope.$on('$stateChangeSuccess', function (event) {

		$window.scrollTo(0, 0);
		$window.ga('send', 'pageview', $location.path());
	});
	$rootScope.$on('$stateChangeStart', function (event) {
		console.log("before route change",$location.url(),$location.search());
		if ($location.search().locale && ['en', 'hi'].indexOf($location.search().locale) != -1) {
			console.log("Already query exist");
		} else if (localStorage.getItem("locale")){
		//	$state.go($location.url(), { lang: localStorage.getItem("locale") }, { locations: 'replace', reload: false, notify: false });
			//$location.search({ lang: localStorage.getItem("locale") });
			console.log("Please set the query");
		}
	});
	$rootScope.locale = (localStorage.getItem('locale')) ? localStorage.getItem('locale') : 'en';
	
	$window.onbeforeunload = function () {
		$window.scrollTo(0, 0);
	}

	// initialize angular-dfp
	dfp();
	$rootScope.locale='en';

	if ($location.search().locale && ['en', 'hi'].indexOf($location.search().locale)!=-1) {
		$rootScope.locale = $location.search().locale;
		localStorage.setItem("locale", $rootScope.locale);
		$rootScope.locale = $rootScope.locale;
		
	} else if (localStorage.getItem("locale")){
		$rootScope.locale = localStorage.getItem("locale");
		
	}else{
		localStorage.setItem("locale", $rootScope.locale);	
		$rootScope.locale = localStorage.getItem("locale");
			
	}


}]);