var express = require('express');
var app = express();
var http = require('http');
var fs = require('fs');
var device = require('express-device');
var httpsEnabled = false;
var redis = require('redis');
var cache = null;
var isProduction = true;

var RDS = {};

if (isProduction) {
    RDS.HOST = "webservicecache.ykhbce.ng.0001.aps1.cache.amazonaws.com",
        RDS.PORT = 6379
} else {
    RDS.HOST = "127.0.0.1",
        RDS.PORT = 6379
}
cache = redis.createClient(RDS.PORT, RDS.HOST).on('connect', function () {
    console.log("REDIS connected");
});

//app.use(compression())
var ENVIRONMENT = ""; //(isProduction) ? "production/" : "";
app.set('view engine', 'ejs');
app.set('view options', {
    layout: false
});
app.set('views', __dirname + '/');
app.use(device.capture());
device.enableDeviceHelpers(app);
device.enableViewRouting(app, {
    "noPartials": false
});

var post = process.env.PORT || 3000;
//app.use(express.static('production'));
app.use(express.static(ENVIRONMENT + 'assets'));
app.use('/desktop', express.static(ENVIRONMENT + 'desktop'));
app.use('/providers', express.static(ENVIRONMENT + 'providers'));
app.use('/mobile', express.static(ENVIRONMENT + 'phone'));
app.use(express.static(ENVIRONMENT + 'phone/views'));
app.use(express.static(ENVIRONMENT + 'desktop/views'));
app.locals.ismin = ""; //(isProduction) ? ".min" : "";
// app.use(express.static('assets'));
// app.use('/desktop', express.static('desktop'));
// app.use('/providers', express.static('providers'));
// app.use('/mobile', express.static('phone'));
// app.use(express.static('phone/views'));
// app.use(express.static('desktop/views'));


/* block SPAM
app.use(function(req,res,next) {
    if(req.get('User-Agent')&&req.get('User-Agent').indexOf("4.0b13")!==-1){
        console.log("SPAM DETECTED ",req.headers['x-forwarded-for']);
        res.send("Thanks for scrapping our URLs. We are happy to inform you that our legal team is taking care of this action so STOP srapping now or serious action will be taken. Happy Coding!!");
    }else{
        next();
    }
 });
 */
app.all('/', function (req, res) {
    if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1&& req.get('User-Agent').indexOf('+http://support.embed.ly') != -1&& req.get('User-Agent').indexOf('+http') != -1)&& req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1) {

        var data = {};
        data.locale = "";
        got('apiv2.spotboye.com/dashboard', {
            json: true
        }).then(response => {
            data.dashboard = response.body.data;
            res.render('bots-template/dashboard.ejs', data);
        }).catch(error => {
            res.status(404).send("Page not found ");
        });
    } else if (req.device.type == "tablet") {
        res.render('index.ejs', {
            forceType: 'phone'
        });
    } else {
        res.render('index.ejs');
    }
});

/** AGENCIES ROUTE START */
var getAgencies=function(req, res,data){
    
    got('apiv2.spotboye.com/agencies/'+data.lang).then(response => {
        data.dashboard = JSON.parse(response.body);
        res.render('bots-template/agencies-dashboard.ejs', data);   
    }).catch(error => {
        res.send(error); 
    });
}
app.get("/:lang?/agencies",function (req, res) {
    var data={};
    data.dashboard=[];
    data.lang=(req.params.lang==null)?"en":"hi";
   
    if(cache){
        cache.get('/agencies/ins/'+data.lang, function (err, _data) {
            if (err||!_data) {
                 getAgencies(req,res,data);
            } else {
                if (_data) {
                    data.dashboard = JSON.parse(_data);
                    res.render('bots-template/agencies-dashboard.ejs', data);   
                } else {
                    getAgencies(req,res,data);
                }
            }
        });
    }else{
        getAgencies(req,res,data); 
    }
});
app.get("/:lang?/agencies/:title?/:id",function (req, res) {
    // http://apiv2.spotboye.com/insarticles/5b9159b970479965555eb6a9
    
    var data={};
    data.article={};
    got('apiv2.spotboye.com/insarticles/'+req.params.id).then(response => {
        data.article = (JSON.parse(response.body)).data;
        res.render('bots-template/agencies-article.ejs', data);   
    }).catch(error => {
        res.send(error); 
    });

});
/** AGENCIES ROUTE END */

var handleBots = function (req, res) {
    var fetchArticle = function () {
        var articleId = req.params.id;
        var data = {}

        got('apiv2.spotboye.com/articlesbot/' + articleId, {
            json: true
        }).then(response => {
            data.article = response.body.data;
            data.locale="";
            data.article.ampURL = data.article.shareURL.replace('.com/', '.com/amp/');
            if(req.locale=="hi"){
                data.article.locale="/hi";
                data.article.shareURL=data.article.shareURL.replace(".com",".com/hi"); 
                data.article.ampURL = data.article.ampURL.replace('.com', '.com/hi');
            }
            res.render('bots-template/article.ejs', data);
        }).catch(error => {
            console.log("handleBots catch",error);
            res.redirect(301,"http://www.spotboye.com");
        });
    }

    if (cache) {
        cache.get("/articlesbot/" + req.params.id, function (err, _article) {
            if (err) {
                console.log("Found err ", err);

                fetchArticle();
            }
            var data = {};
            if (_article) {
                console.log("Got article..");

                data.article = JSON.parse(_article);
                data.article.ampURL = data.article.shareURL.replace('.com/', '.com/amp/');
                res.render('bots-template/article.ejs', data);
            } else {
                fetchArticle();
            }

        });
    } else {
        fetchArticle();
    }



};
app.get("/:catid/:subcat/:title/:id", function (req, res) {
    //handleBots(req,res);
    req.locale = 'en';    
    if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1|| req.get('User-Agent').indexOf('+http://support.embed.ly') != -1|| req.get('User-Agent').indexOf('+http') != -1||req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1||req.get('User-Agent').indexOf('facebookexternalhit')!=-1) { //
        handleBots(req, res);
    } else {
        handleAnything(req, res);
    }

});

app.get("/hi/:catid/:subcat/:title/:id", function (req, res) {
    //handleBots(req,res);
    req.locale = 'hi';
    if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1|| req.get('User-Agent').indexOf('+http://support.embed.ly') != -1|| req.get('User-Agent').indexOf('+http') != -1||req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1||req.get('User-Agent').indexOf('facebookexternalhit')!=-1) {
        handleBots(req, res);
    } else {
        handleAnything(req, res);
    }

});

app.all('/search/:keyword', function (req, res) {
    if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1&& req.get('User-Agent').indexOf('+http://support.embed.ly') != -1&& req.get('User-Agent').indexOf('+http') != -1)&& req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1) {

        var data = {};
        data.locale = "";
        data.keyword=req.params.keyword;
        got('https://apiv2.spotboye.com/en/search/'+req.params.keyword, {
            json: true
        }).then(response => {
            
            data.results= response.body.data;
            if(data.results.isEmpty==false){
                data.results.news=data.results.news.concat(data.results.photos);
                data.results.news=data.results.news.concat(data.results.videos);
            }
            res.render('bots-template/search.ejs', data);
        }).catch(error => {
            res.status(200).send(error);
        });
    } else if (req.device.type == "tablet") {
        res.render('index.ejs', {
            forceType: 'phone'
        });
    } else {
        res.render('index.ejs');
    }
});
app.all('/search', function (req, res) {
    if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1&& req.get('User-Agent').indexOf('+http://support.embed.ly') != -1&& req.get('User-Agent').indexOf('+http') != -1)&& req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1) {

        var data = {};
        data.locale = "";
        data.keyword=req.query.q;
        got('https://apiv2.spotboye.com/en/search/'+req.query.q, {
            json: true
        }).then(response => {
            
            data.results= response.body.data;
            if(data.results.isEmpty==false){
                data.results.news=data.results.news.concat(data.results.photos);
                data.results.news=data.results.news.concat(data.results.videos);
            }
            res.render('bots-template/search.ejs', data);
        }).catch(error => {
            res.status(404).send("Page not found ");
        });
    } else if (req.device.type == "tablet") {
        res.render('index.ejs', {
            forceType: 'phone'
        });
    } else {
        res.render('index.ejs');
    }
});
app.all('/celebrity/:celebname', function (req, res) {
    if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1&& req.get('User-Agent').indexOf('+http://support.embed.ly') != -1&& req.get('User-Agent').indexOf('+http') != -1)&& req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1) {

        var data = {};
        data.locale = "";
        data.profile={};
        got('https://apiv2.spotboye.com/celebrities/'+req.params.celebname, {
            json: true
        }).then(response => {
            data.profile= response.body.data;
           
            res.render('bots-template/celebrity.ejs', data);
        }).catch(error => {
            res.status(404).send("Page not found ");
        });
    } else if (req.device.type == "tablet") {
        res.render('index.ejs', {
            forceType: 'phone'
        });
    } else {
        res.render('index.ejs');
    }
});



app.all("/healthcheck", function (req, res) {
    res.send("OK");
});
app.all("/googlesitemap.xml", function (req, res) {
    res.setHeader("content-type", "application/xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80,
        path: '/googlesitemap.xml'
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});
app.all("/hi/googlesitemap.xml", function (req, res) {
	res.setHeader("content-type", "application/xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80, //80,
		path: '/hi/googlesitemap.xml'
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});

app.all("/celebrity/sitemap.xml", function (req, res) {
    res.setHeader("content-type", "application/xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80, //80,
        path: '/celebrity-sitemap.xml'
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});
app.all("/:cat/sitemap.xml", function (req, res) {
	res.setHeader("content-type", "application/xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80, //80,
		path: '/' + req.params.cat + '/sitemap.xml'
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/hi/:cat/sitemap.xml", function (req, res) {
	res.setHeader("content-type", "application/xml");
	http.get({
		host: 'apiv2.spotboye.com',
		port: 80, //80,
		path: '/hi/' + req.params.cat + '/sitemap.xml'
	}, function (_res) {
		var _data = "";
		_res.on('data', function (chunk) {
			_data += chunk;
		});
		_res.on('end', function () {
			res.send(_data);
		});
	}).on('error', function (e) {
		res.send(e);
	});
});
app.all("/:cat/:sub/sitemap.xml", function (req, res) {
    res.setHeader("content-type", "application/xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80, //80,
        path: '/' + req.params.cat + '/' + req.params.sub + '/sitemap.xml'
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});
app.all("/hi/:cat/:sub/sitemap.xml", function (req, res) {
    res.setHeader("content-type", "application/xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80, //80,
        path: '/hi/' + req.params.cat + '/' + req.params.sub + '/sitemap.xml'
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});
app.all("/sitemap.xml", function (req, res) {
    res.setHeader("content-type", "application/xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80, //80,
        path: '/sitemap.xml'
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});
app.all("/generic-sitemap.xml", function (req, res) {
    res.setHeader("content-type", "application/xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80, //80,
        path: '/generic-sitemap.xml'
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});

app.all("/feed/:id/:vendor", function (req, res) {
    res.setHeader("content-type", "application/rss+xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80,
        path: '/feed/' + req.params.id + '/' + req.params.vendor
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});
app.all("/feed/:vendor", function (req, res) {
    res.setHeader("content-type", "application/rss+xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80,
        path: '/feed/' + req.params.vendor
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});

app.all("/hindi/feed/:id/:vendor", function (req, res) {
    res.setHeader("content-type", "application/rss+xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80,
        path: '/hi/feed/' + req.params.id + '/' + req.params.vendor
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});
app.all("/hindi/feed/:vendor", function (req, res) {
    res.setHeader("content-type", "application/rss+xml");
    http.get({
        host: 'apiv2.spotboye.com',
        port: 80,
        path: '/hi/feed/' + req.params.vendor
    }, function (_res) {
        var _data = "";
        _res.on('data', function (chunk) {
            _data += chunk;
        });
        _res.on('end', function () {
            res.send(_data);
        });
    }).on('error', function (e) {
        res.send(e);
    });
});


// /* ROUTE
var handleAnything = function (req, res) {
    var file = (req.locale && req.locale == 'hi') ? 'index-hi.ejs' : 'index.ejs';
    if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1)||req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1) {
        res.render(file, {
            forceType: 'phone'
        });
    } else
    if (req.device.type == "tablet") {
        res.render(file, {
            forceType: 'phone'
        });
    } else {

        res.render(file);
    }

}

app.get("/.well-known/assetslinks.json", function (req, res) {
    var output = [{
        relation: ["delegate_permission/common.handle_all_urls"],
        target: {
            namespace: "android_app",
            package_name: "com.ninexe.ui",
            sha256_cert_fingerprints: ["DD:EC:4A:47:3D:52:2E:46:53:9D:C6:04:EA:C7:4D:2D:74:09:A6:04:78:9B:4A:5C:19:69:B4:59:0B:49:A8:D5"]
        }
    }];
    res.json(output);
});
app.get("/article/:id", function (req, res) {
    var articleId = req.params.id;
    var data = {
        forceType: 'desktop'
    }
    got('apiv2.spotboye.com/article/' + articleId, {
        json: true
    }).then(response => {
        data.article = response.body.data;
        res.render('amp-template/article-ucbrowser.ejs', data);
    }).catch(error => {
        res.status(502).send("Couldn't able to load")
    });


});
app.get("/hi/article/:id", function (req, res) {
    var articleId = req.params.id;
    var data = {
        forceType: 'desktop'
    }
    got('apiv2.spotboye.com/article/' + articleId, {
        json: true
    }).then(response => {
        data.article = response.body.data;
        res.render('amp-template/article-ucbrowser.ejs', data);
    }).catch(error => {
        res.status(502).send("Couldn't able to load")
    });


});


app.all('/category/*', function (req, res) {
    var url_param = "";
    if (req.params[0]) {
        url_param += "/" + req.params[0]
    }
    if (req.params[1]) {
        url_param += "/" + req.params[1]
    }
    res.redirect(301, "https://" + req.headers.host + url_param);
});
/** AMP RELATED ***/
var got = require('got');


String.prototype.replacerec = function (pattern, what) {
    var newstr = this.replace(pattern, what);
    if (newstr == this)
        return newstr;
    return newstr.replacerec(pattern, what);
};

function getClosingTags(htmlString) {
    return (htmlString.match(/<p/g) || []).length - (htmlString.match(/<\/p>/g) || []).length;
}

function moveTagToRoot(match, p1, offset, htmlString) {
    var closingTags = getClosingTags(htmlString.substring(0, offset));
    var replacement = p1;
    for (var i = 1; i <= closingTags; i++) {
        replacement = "</p>" + replacement + "<p>";
    }
    return replacement;
}

function blockquoteSrc(match, p1, offset, htmlString) {
    var replacement = p1;
    if (replacement.match(/twitter/g)) {
        replacement = replacement.replace(/<script([^>]*)src=(""|'')/g, '<script$1src="https://platform.twitter.com/widgets.js"');
    }
    return '<iframe>' + replacement + '</iframe>';
}
var article_amp = function (req, res) {
    var data = {
        forceType: 'desktop'
    };
    var menu = [];
    var current_url = 'https://' + req.get('host') + req.originalUrl.replace('/amp', '');

    var _fetch_menus = function () {
        got('apiv2.spotboye.com/appMenus', {
            json: true
        }).then(response => {
           // console.log("Got menus API", response.body);
            cache.set('appmenus/raw', JSON.stringify(response.body.data));
            menu = response.body.data;
            _fetch_article();
        }).catch(error => {
            _fetch_article();
        });
    }
    //cache for menus
    if (cache) {
        cache.get('appmenus/raw', function (err, _menu) {
            if (err) {
                _fetch_menus();
            } else {
               // console.log("Got cache menus", _menu);
                if (_menu) {
                    menu = JSON.parse(_menu);
                    _fetch_article();
                } else {
                    _fetch_menus();
                }

            }
        });
    } else {
        _fetch_menus();
    }

    var _fetch_article = function () {
        var renderPage = function (_data) {
            data.article = _data;
           // console.log(data.article.body)
            data.menus = menu;
            //console.log(data.menus);
            var styleAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style=".*?"/g,
                styleDisabledAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style_disabled=".*?"/g,
                imgTag = /<img([^>]*?>)/g,
                border = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)border=".*?"/g,
                shape = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)v:shapes=".*?"/g,
                ampOpenTag = /<(iframe|video)([^>]*?>)/g,
                ampCloseTag = /<\/(iframe|video)>/g,
                youtubeEmbed = /<iframe[^>]*?src="(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:(?:youtube\.com|youtu.be))(?:\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(?:\S+)?"[^>]*?><\/iframe>/g,
                twitterEmbed = /<iframe>(<blockquote)((?:(?!blockquote).)*(?:https?:\/\/twitter\.com\/(?:#!\/)?(?:\w+)\/status(?:es)?\/(\d+))(?:(?!blockquote).)*<\/blockquote>)\s*(?:<script[^>]*?><\/script>)*<\/iframe>/g,
                instagramEmbed = /<iframe(?:(?!iframe).)*instagram\.com\/p\/([^\/]*)\/(?:(?!iframe).)*<\/iframe>/g,
                facebookEmbed = /<iframe[^>]*fbEmbed\?videoId=(\d+)[^>]*><\/iframe>/,
                script = /<script[^>]*>(?:(?!script).)*<\/script>/g;
            var modifiedhtmlString = getBody(data.article.body)
                .replace(youtubeEmbed, '<amp-youtube data-videoid="$1" layout="responsive" width="480" height="270"></amp-youtube>')
                .replace(twitterEmbed, '<amp-twitter width=486 height=657 layout="responsive" data-tweetid="$3">$1 placeholder$2</amp-twitter>')
                .replace(instagramEmbed, '<amp-instagram data-shortcode="$1" width="400" height="400" layout="responsive"></amp-instagram>')
                .replace(facebookEmbed, '<amp-facebook width=552 height=574 layout="responsive" data-embed-as="video" data-href="https://www.facebook.com/facebook/videos/$1/"></amp-facebook>')
                .replace(styleAttribute, '$1')
                .replace(styleDisabledAttribute, '$1')
                .replace(border, '$1')
                .replace(shape,"$1")
                .replace(imgTag, '<amp-img class="contain" width="1.33" height="1" layout="responsive" $1<\/amp-img>')
                .replace(ampOpenTag, '<amp-$1$2')
                .replace(ampCloseTag, '<\/amp-$1>')
                .replace(script, '');
            console.log("***********",styleDisabledAttribute);
            data.article.body = modifiedhtmlString;
            data.current_url = current_url;
            res.render('amp-template/article.ejs', data);
        }
        var _req = function () {
            got('apiv2.spotboye.com/article/' + req.params.id, {
                json: true
            }).then(response => {

                if (response.body.data instanceof Array) {
                    res.render('amp-template/not-found.ejs', data);
                } else {
                    renderPage(response.body.data);
                }

            }).catch(error => {
                console.log('ERRRORR ARTICLE ID'+req.params.id,error);
                res.render('amp-template/not-found.ejs', data);
            });
        }
        if (cache) {
            cache.get("/articlesbot/" + req.params.id, function (err, result) {
                if (err) {
                    _req();
                } else {
                    if (result) {
                        renderPage(JSON.parse(result));
                    } else {
                        _req();
                    }

                }
            });
        } else {
            _req();
        }
    }

}
var article_amp_hi = function (req, res) {
    var data = {
        forceType: 'desktop'
    };
    var menu = [];
    var current_url = 'https://' + req.get('host') + req.originalUrl.replace('/amp', '');

    var _fetch_menus = function () {
        got('apiv2.spotboye.com/appMenus', {
            json: true
        }).then(response => {
           // console.log("Got menus API", response.body);
            cache.set('appmenus/raw', JSON.stringify(response.body.data));
            menu = response.body.data;
            _fetch_article();
        }).catch(error => {
            _fetch_article();
        });
    }
    //cache for menus
    if (cache) {
        cache.get('appmenus/raw', function (err, _menu) {
            if (err) {
                _fetch_menus();
            } else {
                //console.log("Got cache menus", _menu);

                if (_menu) {
                    menu = JSON.parse(_menu);
                    _fetch_article();
                } else {
                    _fetch_menus();
                }

            }
        });
    } else {
        _fetch_menus();
    }

    var _fetch_article = function () {
        var renderPage = function (_data) {
            data.article = _data;
            //console.log(data.article.body)
            data.menus = menu;
            //console.log(data.menus);
            var styleAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style=".*?"/g,
                styleDisabledAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style_disabled=".*?"/g,
                imgTag = /<img([^>]*?>)/g,
                border = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)border=".*?"/g,
                shape = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)v:shapes=".*?"/g,
                ampOpenTag = /<(iframe|video)([^>]*?>)/g,
                ampCloseTag = /<\/(iframe|video)>/g,
                youtubeEmbed = /<iframe[^>]*?src="(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:(?:youtube\.com|youtu.be))(?:\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(?:\S+)?"[^>]*?><\/iframe>/g,
                twitterEmbed = /<iframe>(<blockquote)((?:(?!blockquote).)*(?:https?:\/\/twitter\.com\/(?:#!\/)?(?:\w+)\/status(?:es)?\/(\d+))(?:(?!blockquote).)*<\/blockquote>)\s*(?:<script[^>]*?><\/script>)*<\/iframe>/g,
                instagramEmbed = /<iframe(?:(?!iframe).)*instagram\.com\/p\/([^\/]*)\/(?:(?!iframe).)*<\/iframe>/g,
                facebookEmbed = /<iframe[^>]*fbEmbed\?videoId=(\d+)[^>]*><\/iframe>/,
                script = /<script[^>]*>(?:(?!script).)*<\/script>/g;
            var modifiedhtmlString = getBody(data.article.body)
                .replace(youtubeEmbed, '<amp-youtube data-videoid="$1" layout="responsive" width="480" height="270"></amp-youtube>')
                .replace(twitterEmbed, '<amp-twitter width=486 height=657 layout="responsive" data-tweetid="$3">$1 placeholder$2</amp-twitter>')
                .replace(instagramEmbed, '<amp-instagram data-shortcode="$1" width="400" height="400" layout="responsive"></amp-instagram>')
                .replace(facebookEmbed, '<amp-facebook width=552 height=574 layout="responsive" data-embed-as="video" data-href="https://www.facebook.com/facebook/videos/$1/"></amp-facebook>')
                .replace(styleAttribute, '$1')
                .replace(imgTag, '<amp-img layout="responsive" $1<\/amp-img>')
                .replace(ampOpenTag, '<amp-$1$2')
                .replace(ampCloseTag, '<\/amp-$1>')
                .replace(script, '');

            data.article.body = modifiedhtmlString;
            data.current_url = current_url;
            res.render('amp-template/article-hi.ejs', data);
        }
        var _req = function () {
            got('apiv2.spotboye.com/article/' + req.params.id, {
                json: true
            }).then(response => {

                if (response.body.data instanceof Array) {
                    res.render('amp-template/not-found.ejs', data);
                } else {
                    renderPage(response.body.data);
                }

            }).catch(error => {
                console.log('ERRRORR ARTICLE ID'+req.params.id,error);
                res.render('amp-template/not-found.ejs', data);
            });
        }
        if (cache) {
            cache.get("/articlesbot/" + req.params.id, function (err, result) {
                if (err) {
                    _req();
                } else {
                    if (result) {
                        renderPage(JSON.parse(result));
                    } else {
                        _req();
                    }

                }
            });
        } else {
            _req();
        }
    }

}

app.all('/amp/:cat/:subcat/:title/:id', article_amp);
app.all('/hi/amp/:cat/:subcat/:title/:id', article_amp_hi);
/** AMP RELATED ENDS ***/

//BOTS HANDLER
/** BOTS */
var handleCategoryBot = function (req, res) {
    var data = {};
    var url = '';
    var _redis_k="";
    var page=1;
    if(req.params.page&&req.params.page>0){
        page=parseInt(req.params.page);
    }
    data.pageurl="https://www.spotboye.com/";
    if (req.params.subcat && req.params.subcat != '') {
        data.pageurl+=req.params.catid+"/"+req.params.subcat+"/page/"+(page+1);
        url = 'apiv2.spotboye.com/dashboardsubcatbot/' + req.params.subcat+'?page='+page;
        data.isParent = false;
        _redis_k="/en/dashboardcatbot/"+req.params.subcat+"/"+page;
      
    } else {
       // console.log("Index of ",req.params.catid,data);
        data.isParent = true;
        data.pageurl+=req.params.catid+"/page/"+(page+1);

        url = 'apiv2.spotboye.com/dashboardcatbot/' + req.params.catid+'?page='+page;
        _redis_k="/en/dashboardcatbot/"+req.params.catid+"/"+page;
    
    }
    var requestData=function(){
        got(url, {
            json: true
        }).then(response => {
            data.category=response.body;
            res.render('bots-template/category.ejs', data);
        }).catch(error => {
            res.redirect(301,"https://www.spotboye.com/");
            
        }); 
    }
    if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1) {
       
    if(cache){
        cache.get(_redis_k, function (err, _data) {
            if (err) {
                requestData();
            } else {
                
                if (_data) {
                    data.category = JSON.parse(_data);
            res.render('bots-template/category.ejs', data);
                   
                } else {
                    requestData();
                }

            }
        });
    }else{
        requestData();
    }
    
    } else {
        req.locale = 'en';
        handleAnything(req, res);
    }

}
var handleHindiCategoryBot = function (req, res) {
    
    var data = {};
    var page=1;
    var _redis_k="";
    if(req.params.page&&req.params.page>0){
        page=parseInt(req.params.page);
    }
    var url = '';
    data.pageurl="https://www.spotboye.com/hi/";
    if (req.params.subcat && req.params.subcat != '') {
        data.pageurl+=req.params.catid+"/"+req.params.subcat+"/page/"+(page+1);
        url = 'apiv2.spotboye.com/dashboardsubcatbot/' + req.params.subcat + "?lang=hi&page="+page;
        data.isParent = false;
        _redis_k="/hi/dashboardcatbot/"+req.params.subcat+"/"+page;

    } else {
        data.pageurl+=req.params.catid+"/page/"+(page+1);
        data.isParent = true;
        url = 'apiv2.spotboye.com/dashboardcatbot/' + req.params.catid + "?lang=hi&page="+page;
        _redis_k="/hi/dashboardcatbot/"+req.params.catid+"/"+page;

    }
   
    var requestData=function(){
        got(url, {
            json: true
        }).then(response => {
            data.category=response.body;

            res.render('bots-template/category.ejs', data);
        }).catch(error => {
            res.redirect(301,"https://www.spotboye.com/hi/");
        }); 
    }
    if (req.device.type == "bot" || req.get('User-Agent').indexOf('WhatsApp') != -1 || req.get('User-Agent').indexOf('+https://developers.google.com/+/web/snippet/') != -1) {
        if(cache){
            cache.get(_redis_k, function (err, _data) {
                if (err) {
                    requestData();
                } else {
                    
                    if (_data) {
                        data.category = JSON.parse(_data);
                res.render('bots-template/category.ejs', data);
                       
                    } else {
                        requestData();
                    }
    
                }
            });
        }else{
            requestData();
        }
    } else {
        req.locale = 'hi';
        handleAnything(req, res);
    }

}

var handleHindiDashboardBot = function (req, res) {
    var data = {};
    data.locale = 'hi/';
    if (req.device.type == "bot" || (typeof req.get('User-Agent') !== "undefined" && req.get('User-Agent').indexOf('PhantomJS') !== -1)||req.get('User-Agent').indexOf('Links (2.8; Linux 4.4.0.43 i586; GNU C 4.8.2; text)')!=-1) {

        got('apiv2.spotboye.com/dashboard?lang=hi', {
            json: true
        }).then(response => {
            cache.set('/hi/dashboardarticle/raw', JSON.stringify(response.body.data));
            data.dashboard = response.body.data;
            res.render('bots-template/dashboard.ejs', data);
        }).catch(error => {
            res.status(404).send("Article not found");
        });

    } else {
      //  console.log("Its anything");
        req.locale = 'hi';
        handleAnything(req, res);
    }
}
var checkSlash=function(req,res,next) {
    if (req.path.substr(-1) == '/' && req.path.length > 1) {
       next();
    }else{
        var query = req.url.slice(req.path.length);
        res.redirect(301, req.path+"/" + query); 
    }
}
app.get('/hi',checkSlash, handleHindiDashboardBot);
app.get("/hi/:catid/",checkSlash, handleHindiCategoryBot);
app.get("/hi/:catid/:subcat/",checkSlash, handleHindiCategoryBot);
app.get("/:catid/",checkSlash, handleCategoryBot);
app.get("/:catid/:subcat/",checkSlash, handleCategoryBot);
app.get("/hi/:catid/page/:page",checkSlash, handleHindiCategoryBot);
app.get("/hi/:catid/:subcat/page/:page",checkSlash, handleHindiCategoryBot);
app.get("/:catid/page/:page",checkSlash, handleCategoryBot);
app.get("/:catid/:subcat/page/:page",checkSlash, handleCategoryBot);



app.all('/*', handleAnything);
/*app.all('/profile', function (req, res) {
  res.render('index.ejs');
});

app.all('/login', function (req, res) {
  res.render('index.ejs');
});*/
app.all('/not-found', function (req, res) {
    res.status(404).send("<h2>Oops! Nothing here :-) </h2>");
});

function shareLocaleURL(str, locale) {
    return str.replace('.com/', '.com/' + locale);
}

function getBody(htmlString) {
    var starsWithDiv = /^(<div>)/g;
    var modifiedhtmlString = starsWithDiv.test(htmlString) ? htmlString : '<div>' + htmlString + '</div>';
    var linebreaks = /\r?\n|\r/g,
        bold = /<b>((?:<br>)*)([^<]+?)((?:<br>)*)<\/b>/g,
        italic = /<i>((?:<br>)*)([^<]+?)((?:<br>)*)<\/i>/g,
        ignoreBIOpen = /<((b)|(i))((\s+[^>]*?)|)>/g,
        ignoreBIClose = /<\/((b)|(i))>/g,
        comments = /<!--[\s\S]*?-->/g,
        ignoredTagsOpen = /<((span)|(font)|(u)|(o:p))((\s+[^>]*?)|)>/g,
        ignoredTagsClose = /<\/((span)|(font)|(u)|(o:p))>/g,
        twttrScript = "<script>twttr.widgets.load();<\/script>",
        divOpen = /(<div)|(<h([1-6]))|(<span)/g,
        divClose = /(<\/div>)|(<\/h([1-6])>)|(<\/span>)/g,
        imgLink = /(<a[^>]*?>)(<img[^>]*?>)(<\/a>)/g,
        imgTag = /(<img[^>]*?>)/g,
        iframeTag = /(<iframe[\s\S^>]*?><\/iframe>)/g,
        instagramTag = /(<iframe(?:(?!iframe).)*instagram(?:(?!iframe).)*height="(\d+)")((?:(?!iframe).)*><\/iframe>)/g,
        pTag = /(<p[^>]*?>(?:(?!(?:<p|<\/p>)).)*<\/p>)/g,
        emptyImgTag = /<img([^>]*?)src=""([^>]*?)>/g,
        emptyPTag = /<p[^>]*?>(<br>)*(<script([^>]*?)><\/script>)*(\s)*<\/p>/g,
        blockquoteTag = /(<blockquote(?:(?!blockquote).)*<\/blockquote>\s*(?:<script([^>]*?)><\/script>)*)/g,
        multipleBr = /(\s*<br>\s*)+/g;
    return modifiedhtmlString
        .replace(linebreaks, " ")
        .replace(comments, "")
        .replace(ignoredTagsOpen, "")
        .replace(ignoredTagsClose, "")
        .replace(twttrScript, "")
        .replace(divOpen, "<p")
        .replace(divClose, "</p>")
        .replace(emptyImgTag, '')
        .replace(blockquoteTag, blockquoteSrc)
        .replace(imgLink, '$1link$3$2')
        .replace(imgTag, moveTagToRoot)
        .replace(iframeTag, moveTagToRoot)
        .replace(instagramTag, '$1 width="$2"$3')
        //.replacerec(pTag, moveTagToRoot)
        .replace(bold, '$1<strong>$2</strong>$3')
        .replace(italic, '$1<em>$2</em>$3')
        .replace(ignoreBIOpen, "")
        .replace(ignoreBIClose, "");
    //.replacerec(multipleBr, '</p><p>')
    //.replacerec(emptyPTag, '');
}

app.listen(post, function () {
    console.log('Listening on port 3000!')
});
