app.config(["$stateProvider", "$locationProvider", "$urlRouterProvider", "$compileProvider", "socialProvider", "$urlMatcherFactoryProvider", function ($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider, socialProvider, $urlMatcherFactoryProvider) {
    $locationProvider.html5Mode(!0), $urlMatcherFactoryProvider.strictMode(0), socialProvider.setFbKey({
            appId: "630344967097986",
            apiVersion: "v2.6"
        });
    localStorage.setItem('locale', 'hi');
        
        $urlRouterProvider.otherwise("/not-found"), $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|whatsapp):/);
    $stateProvider.state("home", {
        url: "/hi",
        templateUrl: "/" + WEBSERVICE.DEVICE + "/views/dashboard.html",
        controller: "homeCtrl",
        params: {
            meta: {
                title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
            }
        }
    }).state("homes", {
        url: "/hi/",
        templateUrl: "/" + WEBSERVICE.DEVICE + "/views/dashboard.html",
        controller: "homeCtrl",
        params: {
            meta: {
                title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
            }
        }
    }).state("search", {
            url: "/hi/search/:q",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/search.html",
            controller: "searchCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state("search_query", {
            url: "/hi/search?q",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/search.html",
            controller: "searchCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state('bolly-games', {
            url: '/hi/games/celebirds',
            templateUrl: '/' + WEBSERVICE.DEVICE + '/views/celebirds.html'
        })
        .state("celebrities", {
            url: "/hi/celebrity",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/celebrities.html",
            controller: "allCelebrityCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state("celebrities-name", {
            url: "/hi/celebrity/:id",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/celebrity.html",
            controller: "celebrityCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state("celebrity", {
            url: "/hi/celebrity/:id",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/celebrity.html",
            controller: "celebrityCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state("login", {
            url: "/hi/login",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/login.html",
            controller: "loginCtrl"
        }).state("profile", {
            url: "/hi/profile",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/profile.html",
            controller: "profileCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state("not-found", {
            url: "/not-found",
            template: "<h3 class='container'>NOT FOUND</h3>"
        }).state("sitemap", {
            url: "/hi/sitemap",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/sitemap.html"
        }).state("terms", {
            url: "/hi/terms",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/terms.html"
        }).state("disclaimer", {
            url: "/hi/disclaimer",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/disclaimer.html"
        }).state("privacypolicy", {
            url: "/hi/privacypolicy",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/privacypolicy.html"
        }).state("contactus", {
            url: "/hi/contactus",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/contactus.html"
        }).state("aboutus", {
            url: "/hi/aboutus",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/about.html"
        }).state("loaderio-2333ad9e33990b93db7e57c2388a376b", {
            url: "/hi/loaderio-2333ad9e33990b93db7e57c2388a376b",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/loaderio-2333ad9e33990b93db7e57c2388a376b.html"
        }).state("logout", {
            url: "/hi/logout",
            controller: function (Storage, $window) {
                Storage.clearUser(), user_info = {
                    islogged: !1,
                    userId: 0
                }, $window.location.href = "/"
            }
        }).state("parentCategory", {
            url: "/hi/:category",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/category.html",
            controller: "categoryCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state("childCategory", {
            url: "/hi/:category/:subcategory",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/category.html",
            controller: "categoryCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        }).state("article", {
            url: "/hi/:category/:subcategory/:title/:id",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/article.html",
            controller: "articlesCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywood Actress and Actors Updates | spotboye.com"
                }
            }
        })
}]);