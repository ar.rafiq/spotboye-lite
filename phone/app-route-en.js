app.config([
    "$stateProvider",
    "$locationProvider",
    "$urlRouterProvider",
    "$compileProvider",
    "socialProvider",
    "$urlMatcherFactoryProvider",
    function ($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider, socialProvider, $urlMatcherFactoryProvider) {
        $locationProvider.html5Mode(!0),
        $urlMatcherFactoryProvider.strictMode(0),
        socialProvider.setFbKey({appId: "630344967097986", apiVersion: "v2.6"});
        localStorage.setItem('locale', 'en');

        $urlRouterProvider.otherwise("/not-found"),
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|whatsapp):/),
        $stateProvider
            .state("home", {
            url: "/",
            templateUrl: "/" + WEBSERVICE.DEVICE + "/views/dashboard.html",
            controller: "homeCtrl",
            params: {
                meta: {
                    title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                            "d Actress and Actors Updates | spotboye.com",
                    description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                            "d Actress and Actors Updates | spotboye.com"
                }
            }
        })
        .state('salman-apology', {
			url: '/apology',
			templateUrl: '/' + WEBSERVICE.DEVICE + '/views/apology.html'
		})
            .state("search", {
                url: "/search/:q",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/search.html",
                controller: "searchCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state("search_query", {
                url: "/search?q",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/search.html",
                controller: "searchCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state('bolly-games', {
                url: '/games/celebirds',
                templateUrl: '/' + WEBSERVICE.DEVICE + '/views/celebirds.html'
            })
            .state("celebrities", {
                url: "/celebrity",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/celebrities.html",
                controller: "allCelebrityCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state("celebrities-name", {
                url: "/celebrity/:id",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/celebrity.html",
                controller: "celebrityCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state("celebrity", {
                url: "/celebrity/:celebrity/:id",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/celebrity.html",
                controller: "celebrityCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state("login", {
                url: "/login",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/login.html",
                controller: "loginCtrl"
            })
            .state("profile", {
                url: "/profile",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/profile.html",
                controller: "profileCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state("not-found", {
                url: "/not-found",
                template: "<h3 class='container'>NOT FOUND</h3>"
            })
            .state("sitemap", {
                url: "/sitemap",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/sitemap.html"
            })
            .state("terms", {
                url: "/terms",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/terms.html"
            })
            .state("disclaimer", {
                url: "/disclaimer",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/disclaimer.html"
            })
            .state("privacypolicy", {
                url: "/privacypolicy",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/privacypolicy.html"
            })
            .state("contactus", {
                url: "/contactus",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/contactus.html"
            })
            .state("aboutus", {
                url: "/aboutus",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/about.html"
            })
            .state("loaderio-2333ad9e33990b93db7e57c2388a376b", {
                url: "/loaderio-2333ad9e33990b93db7e57c2388a376b",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/loaderio-2333ad9e33990b93db7e57c2388a376b.html"
            })
            .state("logout", {
                url: "/logout",
                controller: function (Storage, $window) {
                    Storage.clearUser(),
                    user_info = {
                        islogged: !1,
                        userId: 0
                    },
                    $window.location.href = "/"
                }
            })
            .state("parentCategory", {
                url: "/:category",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/category.html",
                controller: "categoryCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state("childCategory", {
                url: "/:category/:subcategory",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/category.html",
                controller: "categoryCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
            .state("article", {
                url: "/:category/:subcategory/:title/:id",
                templateUrl: "/" + WEBSERVICE.DEVICE + "/views/article.html",
                controller: "articlesCtrl",
                params: {
                    meta: {
                        title: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com",
                        description: "Bollywood News and Gossip | Bollywood Movie Reviews, Songs and Videos | Bollywoo" +
                                "d Actress and Actors Updates | spotboye.com"
                    }
                }
            })
    }
]);