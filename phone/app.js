function expandShowAll(elm) {
    $(elm).prev().toggleClass("expanded_active")
}

function toggleIcon(e) {}

function changeMenuColor(elm) {
    $(elm).find(".more-less").toggleClass("glyphicon-plus glyphicon-minus"), $(".submenu").css("background-color", $(elm).css("background-color"))
}

function openNav() {
    document.getElementById("sideNav").style.right = "0%", $("body").css("overflow", "hidden")
}

function closeNav() {
    document.getElementById("sideNav").style.right = "-100%", $("body").css("overflow", "auto")
}
var app = angular.module("base", ["ui.router", "slickCarousel", "socialLogin", "angularLazyImg"]),
    user_info = {
        islogged: !1,
        userId: 0
    };
WEBSERVICE.DEVICE = "mobile", app.service("Storage", Storage), app.factory("APIService", APIService);
 app.filter("slug", function () {
    return function (x) {
        return x && x.length ? (x = x.replace(/[^a-zA-Z0-9-_]/g, "-"), x = x.replace(/-+/g, "-").toLowerCase(), "-" == x[x.length - 1] && (x = x.slice(0, x.length - 1)), x) : ""
    }
});
app.filter("locale", function () {
    return function (x,locale) {
        if(locale&&x){
            return x.replace('.com','.com'+locale);
        }else{
            return x;
        }
    }
});
 app.filter("timeago", function () {
    return function (x) {
        return getMomentAgo(new Date(x).getTime())
    }
}), app.filter("kViewers", function () {
    return function (x) {
        return parseInt(x) > 999 ? (parseInt(x) / 1e3).toFixed(1) + "K" : x
    }
});
app.filter('urlEncode', function () {
    return function (input) {
        if (input) {
            return window.encodeURIComponent(input);
        }
        return "";
    }
});

app.run(["$rootScope", "$location", "$window",  function ($rootScope, $location, $window) {
   
    $window.ga("create", "UA-53522784-1", "auto");
    $window.ga("require", "GTM-TMRTK62");

    $rootScope.$on("$stateChangeSuccess", function (event) {
        //  <!-- Google Tag Manager -->
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TNGW4LC');
        //    <!-- End Google Tag Manager -->
        $window.ga("send", "pageview", $location.path())
    }), $window.onbeforeunload = function () {
        $window.scrollTo(0, 0)
    }

    $rootScope.locale = (localStorage.getItem('locale')) ? localStorage.getItem('locale') : 'en';
    $rootScope.localeURL = ($rootScope.locale != 'en') ? '/' + $rootScope.locale : '';

    // initialize angular-dfp
  //  dfp();
    $rootScope.changeLocale = function (v) {
        localStorage.setItem('locale', v);
        var _p = (v == 'hi') ? 'hi/' : '';
        window.location = window.location.protocol + "//" + window.location.host + "/" + _p;
    };
}]);


app.directive("articles", function () {
    return {
        restrict: "E",
        templateUrl: "/" + WEBSERVICE.DEVICE + "/templates/articles.html",
        scope: {
            content: "=",
            listType: "@"
        },
        controller: "articlesPartial"
    }
});

app.directive('staquAd', function ($http) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/' + WEBSERVICE.DEVICE + '/templates/staqu.html',
        scope: {
            src: '@'
        },
        link: function postLink(scope, elem, attrs) {
            scope.loaded = false;
            scope.slick = {};
            scope.slick.curIndex = 0;
            scope.slick.data = [];
            scope.slick.dots = true;
            scope.slick.prevSlide = function () {
                if (scope.slick.curIndex > 0) {
                    scope.slick.curIndex--;

                } else {
                    scope.slick.curIndex = 0; //for infinite scroll
                }
            }
            scope.slick.nextSlide = function () {
                if (scope.slick.curIndex < scope.slick.data.length - 1) {
                    scope.slick.curIndex++;
                } else {
                    scope.slick.curIndex = 0; //for infinite scroll
                }
            }
            $http.get('https://videos.snapnbuy.in/api/v1/image-recom?image_url=' + scope.src).then(function (res) {
                scope.loaded = true;
                res.data.results.forEach(function (_parent, _pI) {
                    _parent.products.forEach(function (_product, _proIndex) {
                        scope.slick.data.push(_product);
                    });
                })
            }, function (err) {
                scope.loaded = true;
            });


        }
    }

});
app.directive("slides", function () {
    return {
        restrict: "E",
        templateUrl: "/" + WEBSERVICE.DEVICE + "/templates/slides.html",
        scope: {
            type: "@",
            contentType: "@",
            slidesData: "=",
            skip: "@"
        },
        controller: "slidesPartial"
    }
}), app.directive("list", function () {
    return {
        restrict: "E",
        templateUrl: "/" + WEBSERVICE.DEVICE + "/templates/list.html",
        scope: {
            content: "=",
            contentType: "@"
        },
        controller: "listPartial"
    }
}), app.directive("poll", function () {
    return {
        restrict: "E",
        templateUrl: "/" + WEBSERVICE.DEVICE + "/templates/poll.html",
        scope: {
            content: "=",
            contentType: "@",
            pageType: "@"
        },
        controller: "pollPartial"
    }
}), app.directive("avatars", function () {
    return {
        restrict: "E",
        replace: !0,
        templateUrl: "/" + WEBSERVICE.DEVICE + "/templates/avatars.html",
        scope: {
            displayType: "@",
            content: "="
        },
        controller: "avatarsPartial"
    }
}), app.directive("moreLess", function () {
    return {
        restrict: "E",
        template: '<div><div ng-if="total>3" ng-init="showExpand=true" onclick="expandShowAll(obj);" ng-click="showExpand=!showExpand"><div class="show_all_expand" ng-show="showExpand"><span>SHOW MORE <i class="glyphicon glyphicon-menu-right"></i></span></div><div class="show_all_expand" ng-show="!showExpand"><span>HIDE MORE <i class="glyphicon glyphicon-menu-right"></i></span></div></div></div>',
        scope: {
            total: "@",
            obj: "="
        }
    }
}), app.directive("fbComments", function () {
    function createHTML(href, numposts, colorscheme) {
        return '<div class="fb-comments" data-href="' + href + '" data-numposts="' + numposts + '" data-colorsheme="' + colorscheme + '"></div>'
    }
    return {
        restrict: "E",
        replace: !0,
        link: function (scope, elem, attrs) {
            attrs.$observe("href", function (newValue) {
                var href = newValue,
                    numposts = attrs.numposts || 5,
                    colorscheme = attrs.colorscheme || "light";
                elem.html(createHTML(href, numposts, colorscheme)), FB.XFBML.parse(elem[0])
            })
        }
    }
});
app.directive('apester', function () {
	return {
		restrict: 'E',
		replace: true,
		link: function postLink(scope, elem, attrs) {
			var slotID = attrs.uid;//newValue;
			slotID=slotID+"_"+Math.round(Math.random()*200)*2;
            setTimeout(function(){
               // <interaction data-token="5b630b109c10f54f2330ee7e" data-context="true" data-tags="" data-fallback="false"></interaction>
					elem.html('<interaction data-token="5b630b109c10f54f2330ee7e" data-context="true" data-tags="" data-fallback="false"></interaction>');
                    // elem.html('<div class="apester-media" id="'+slotID+'"  data-media-id="5b62f2aac299f782941287ea" height="512"></div>');
				}, 300);
		}
	}
});
app.directive('columbiaAd', function () {
	return {
		restrict: 'E',
		replace: true,
		link: function postLink(scope, elem, attrs) {
			var slotID = attrs.uid;//newValue;
			slotID=slotID+"_"+Math.round(Math.random()*200)*2;
            setTimeout(function(){
                colombia.fns.push(function(){
                    elem.html('<div id="div-clmb-ctn-211040-1" style="min-height:2px;width:100%;" data-slot="211040" data-position="1" data-section="0" data-ua="m" class="colombia"></div>');
						colombia.refresh("div-clmb-ctn-211040-1")
					 });		
				}, 300);
		}
	}
});
app.directive('googleAds', function () {
    return {
        restrict: 'E',
        replace: true,
        link: function postLink(scope, elem, attrs) {
          //  attrs.$observe('id', function (newValue) {
                var slotID = attrs.id;//newValue;
                slotID=slotID+"_"+Math.round(Math.random()*3)*2;
                elem.html('<div id="' + slotID + '"></div>');
                var slot = {};
                
                setTimeout(function(){    googletag.cmd.push(function () {

                        switch (attrs.type) {
                            case 'HP_320x100_ATF':
                           // googletag.defineSlot('/97172535/Spotboye_Mobile_HP_320x100_ATF', [320, 100], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_HP_320x100_ATF', [320,100],slotID);
                            break;
                            case 'HP_300x250_Mid':
                           // googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_Mid', [300, 250], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_HP_300x250_Mid', [300,250],slotID);
                            break;
                            case 'HP_300x250_BTF':
                            //googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF', [300, 250], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_HP_300x250_BTF', [300,250],slotID);
                            break;
                            case 'HP_300x250_BTF2':
                           // googletag.defineSlot('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_HP_300x250_BTF2', [300,250],slotID);
                            break;
                           
                            case 'AP_320x100_ATF':
                           // googletag.defineSlot('/97172535/Spotboye_Mobile_AP_320x100_ATF', [320, 100], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_AP_320x100_ATF', [320,100],slotID);
                            break;
                            case 'AP_300x250_BTF':
                          //  googletag.defineSlot('/97172535/Spotboye_Mobile_AP_300x250_BTF', [300, 250], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_AP_300x250_BTF', [300,250],slotID);
                            break;
                            case 'AP_300x250_BTF2':
                           // googletag.defineSlot('/97172535/Spotboye_Mobile_AP_300x250_BTF2', [300, 250], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_AP_300x250_BTF2', [300,250],slotID);
                            break;
                            case 'AP_300x250_Mid':
                            //googletag.defineSlot('/97172535/Spotboye_Mobile_AP_300x250_Mid', [300, 250], slotID).addService(googletag.pubads());
                            googletag.pubads().setTargeting('pagetype', ['test']).display('/97172535/Spotboye_Mobile_AP_300x250_Mid', [300,250],slotID);
                            break;
                            
                            case 'OOP_Mobile_Native':
                            googletag.defineOutOfPageSlot('/97172535/Spotboye.com_Mobile_Native', slotID)
                            .addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                            break;

/*
                            case 'Ad1_1_300X250':
                                slot = googletag.defineSlot('/97172535/Test1_adunit_300x250', [300, 250], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                 break;
                            case 'Ad1_2_300X250':
                                slot = googletag.defineSlot('/97172535/HP_RANDOMBANNER_300X250', [300, 250], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                 break;
                            case 'Ad1_3_300X250':
                                slot = googletag.defineSlot('/97172535/HP_RANDOMBANNER1_300X250', [300, 250], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                 break;

                            case 'Ad2_3_MBTF_300X250':
                                slot = googletag.defineSlot('/97172535/MOBILE_ARICLE_2_320X250', [300, 250], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                 break;

                            case 'Ad2_2_LB_728X90':
                                slot = googletag.defineSlot('/97172535/SECTION1_728X90', [320, 50], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                 break;
                            case 'Ad2_3_LB_728X90':
                                slot = googletag.defineSlot('/97172535/SECTION3_728X90', [320, 50], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                 break;
                            case 'Ad2_3_BTF_LB_728X90':
                                slot = googletag.defineSlot('/97172535/SECTION_7_728X90', [320, 50], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                 break;
                            case 'Ad2_1_LB_728X90':
                                slot = googletag.defineSlot('/97172535/Test2_adunit_728X90', [320, 50], slotID).setTargeting("article", "infinitescroll").addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                break;
                            case 'Ad2_2_BTF_LB_728X90':
                                slot = googletag.defineSlot('/97172535/SECTION_5_728X90', [320, 50], slotID).setTargeting("article", "infinitescroll").addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                break;
                            case 'Ad2_3_BTF_LB_728X90':
                                slot = googletag.defineSlot('/97172535/SECTION_7_728X90', [320, 50], slotID).setTargeting("article", "infinitescroll").addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                               break;
*/
                            case 'interstitial':
                                slot = googletag.defineOutOfPageSlot('/97172535/Interstitial_Adunit', slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                break;

                            case 'vibe':
                                slot = googletag.defineSlot('/97172535/Vbe_In-article_1x1', [1, 1], slotID).addService(googletag.pubads());
                                googletag.pubads();
                        googletag.enableServices();
                        googletag.display(slotID);
                        break;

                            case 'Ad1_1_BTF_300X250':
                                slot = googletag.defineSlot('/97172535/MOBILE_ARICLE_1_320X250', [300, 250], slotID).addService(googletag.pubads());
                                googletag.pubads();
                        googletag.enableServices();
                        googletag.display(slotID);
                        break;

                            case 'mcanvas':
                                slot = googletag.defineSlot('/97172535/mCanvas', [1, 1], slotID).addService(googletag.pubads());
                                googletag.pubads();
                                googletag.enableServices();
                                googletag.display(slotID);
                                break;
                        }

                        
                        // googletag.pubads().refresh([slot]);
                    });
                },300);
           // });
        }
    };
});

app.directive("videoAds", function ($location) {
        var getRandomUrl = function () {
            var _url = ["https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=285&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__", "https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=286&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__", "https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=287&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__", "https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=288&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__", "https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=289&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__", "https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=290&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__", "https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=292&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__", "https://saxp.zedo.com/jsc/xp2/fns.vast?n=3669&c=291&d=26&s=22&v=vast2&pu=__page-url__&ru=__referrer__&pw=__player-width__&ph=__player-height__&z=__random-number__"];
            return _url[Math.floor(Math.random() * +_url.length)]
        };
        return {
            restrict: "E",
            replace: !0,
            link: function (scope, elem, attrs) {
                var slotID = "video_" + (new Date).getTime() + "_" + Math.floor(6 * Math.random()) + 1,
                    _html = "<div style='z-index:2;position:absolute;padding:15px;color:#fff;font-size: 35px' id='mute-controls-" + slotID + "'><i class='fa fa-volume-off' aria-hidden='true'></i></div><video style='width:300px;height:171px;margin-bottom:16px' id='" + slotID + "' class='video-js' muted preload='auto'> <source src='http://production.smedia.lvp.llnw.net/0a0249c68c4b4fce8d4ba9730dc59c9c/vB/OlWXEpGQJDiCnpKiyH6sA7WVn9j1UcFOaUYoqvOo0/sanam-abigail-interview_jw.mp4?x=0&h=0a40c5304f09727d7792b3eedb6839a8' type='video/mp4'><p class='vjs-no-js'> To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video</p></video>";
                elem.html(_html);
                var player = videojs(slotID),
                    options = {
                        id: slotID,
                        adTagUrl: getRandomUrl()
                    };
                player.ima(options), player.ima.initializeAdDisplayContainer(), player.ima.requestAds(), player.muted(!0), player.play(), elem.find("#mute-controls-" + slotID).on("click", function () {
                    player.muted() ? (player.muted(!1), $(this).html("<i class='fa fa-volume-up' aria-hidden='true'></i>")) : (player.muted(!0), $(this).html("<i class='fa fa-volume-off' aria-hidden='true'></i>"))
                })
            }
        }
    }), app.directive("videoPlayer", function ($window) {
        return {
            restrict: "E",
            replace: !0,
            link: function (scope, elem, attrs) {
                attrs.$observe("href", function (src) {
                    var slotID = "video_" + (new Date).getTime() + "_" + Math.floor(6 * Math.random()) + 1,
                        _html = "<video id='" + slotID + "' class='video-js' controls preload='auto' poster='" + attrs.thumbnail.replace("http:","https:") + "' data-setup='{}'> "; - 1 != src.indexOf(".m3u8") ? _html += "<source src='" + src + "' type='application/x-mpegURL'>" : _html += "<source src='" + src + "' type='video/mp4'>", _html += "<p class='vjs-no-js'> To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video</p></video>", elem.html(_html);
                    var player = videojs(slotID),
                        options = {
                            id: slotID,
                            adTagUrl: "https://playlist.ventunotech.com/api/publisher/getAd.php?admode=preroll&key=555346b492236&pageurl=[page_url]&content_id=[content_id]&content_title=[content_title]&category=[content_category]&content_keywords=[content_keywords]&content_URL=[content_URL]&mute_status=[MUTE_STATUS]&player_width=400&player_height=225&autoplay_status=[AUTOPLAY_STATUS]&player_position=[PLAYER_POSITION]&n=[TIMESTAMP]&ad_type=VPAID2.0"
                        };
                    player.ima(options);
                    document.getElementById("content_video_html5_api");
                    player.one("click", function () {
                        player.ima.initializeAdDisplayContainer(), player.ima.requestAds(), player.play()
                    }), $window.__ventunoplayer = $window.__ventunoplayer || [], $window.__ventunoplayer.push({
                        publisher_key: "555346b492236",
                        slot_id: slotID,
                        holder_id: slotID,
                        player_type: "ep",
                        async: "1"
                    })
                })
            }
        }
    }), app.controller("articlesPartial", ["$rootScope", "$scope", "$state", "$filter", function ($rootScope, $scope, $state, $filter) {
        $scope._random = function () {
            $scope._r = Math.floor(100 * Math.random()) + 1
        };
    $scope.localeURL = $rootScope.localeURL;
        $scope.data = [], $scope.content instanceof Array ? $scope.data = $scope.content : $scope.data.push($scope.content), $scope.openArticle = function (section) {
            var _title =section.slug;
            $state.go("article", {
                category: section.mainSection.parentSlug,
                subcategory: section.mainSection.slug,
                title: _title,
                id: section._id
            });
        }, $scope.Share = {}, $scope.Share.fb = function (url) {
            $rootScope.Share.fb(url);
        }
    }]), app.controller("listPartial", ["$rootScope", "$scope", "$state", "$filter", function ($rootScope, $scope, $state, $filter) {
    $scope.localeURL = $rootScope.localeURL;
        $scope.data = [], $scope.content instanceof Array ? $scope.data = $scope.content : $scope.data.push($scope.content), $scope.openArticle = function (section) {
            var _title =section.slug;
            $state.go("article", {
                category: section.mainSection.parentSlug,
                subcategory: section.mainSection.slug,
                title: _title,
                id: section._id
            })
        }
    }]), app.controller("pollPartial", ["$scope", "$state", "APIService", "$window", "$rootScope", "$filter", function ($scope, $state, APIService, $window, $rootScope, $filter) {
    $scope.localeURL = $rootScope.localeURL;

        $scope.slickConfig = {
            enabled: !0,
            autoplay: !1,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            event: {}
        };
         $scope.data = {}, $scope.windowHeight = $window.outerHeight - 50;
          $scope.currentIndex = 0;
           $scope.isSlide = !1;
            $scope.content instanceof Array ? ($scope.isSlide = !0, $scope.data = $scope.content) : ($scope.isSlide = !1, $scope.data = $scope.content);
             $scope.poll = {}, $scope.display_txt = "";
              $scope.poll.vote = function (index, question, option) {
            var _answers = [],
                articleId = $scope.data[index]._id;
            _answers.push({
                questionId: question,
                optionId: option
            }), $scope.display_txt = "Voting! please wait..";
            var _poll_param = {
                answers: _answers,
                articleId: articleId
            };
            user_info.islogged && (_poll_param.userId = user_info.userId);
             APIService.poll(_poll_param, function (_d) {
                _d.success ? ($scope.data[index].results = _d.answerPercent, $scope.data[index].attempted = 1, $scope.display_txt = "") : $scope.display_txt = "Error occurred please try again"
            })
        };
        var _slick = {};
        $scope.contestConfig = {
            enabled: !0,
            method: {},
            event: {
                init: function (event, slick) {
                    _slick = slick
                }
            }
        }, $scope.contest = {};
         $scope.display_txt = "";
          $scope.contest.answer = function ($index, qId, aId, nextQuestion) {

            $rootScope.loginRequire($scope.data.mainSection.parentSlug + '/' + $scope.data.mainSection.slug + '/' +$scope.data.slug + '/' + $scope.data._id);


            $scope.data._answers = $scope.data._answers ? $scope.data._answers : [];
             $scope.data._answers.push({
                questionId: qId,
                optionId: aId
            });
             nextQuestion += 1;
              $scope.data.questions.length > nextQuestion ? $scope.data.currentIndex = nextQuestion : _contestVote($index, {
                answers: $scope.data._answers,
                articleId: $scope.data._id
            })
        };
        var _contestVote = function (i, _p) {
            $scope.display_txt = "Submitting your answer..";
            var _contest_param = _p;
            user_info.islogged && (_contest_param.userId = user_info.userId);
             APIService.poll(_contest_param, function (_d) {
                _d.success ? ($scope.data.results = _d.points, $scope.data.attempted = 1, $scope.display_txt = "") : $scope.display_txt = "Error occurred please try again"
            })
        }
    }]), app.controller("avatarsPartial", ["$state", "$scope", "$state","$filter", function ($state, $scope, $state,$filter) {
        $scope.data = [], $scope.content instanceof Array ? $scope.data = $scope.content : $scope.data.push($scope.content), $scope.viewCelebrity = function (id) {
            id=$filter('slug')(id);
            $state.go("celebrity", {
                id: id
            })
        }
    }]), app.controller("slidesPartial", ["$scope",  "$state","$rootScope", function ($scope, $state,$rootScope) {
    $scope.localeURL = $rootScope.localeURL;
    $scope._random = function () {
        $scope._r = Math.floor(100 * Math.random()) + 1
    }; 
        $scope.slides = [], $scope.currentIndex = 0, $scope.videoCurrentIndex = 0, $scope.videoReviewCurrentIndex = 0, $scope.trendingCurrentIndex = 0, $scope.photoCurrentIndex = 0, $scope.slickConfig = {
            enabled: !0,
            autoplay: !0,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            dots: !0,
            arrows: !1,
            centerMode: !0,
            infinite: !1,
            event: {
                afterChange: function (event, slick, currentSlide, nextSlide) {
                    $scope.currentIndex = currentSlide
                },
                init: function (event, slick) {
                    slick.slickGoTo($scope.currentIndex)
                }
            }
        }, $scope.videoReviewSlickConfig = {
            enabled: !0,
            autoplay: !0,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            dots: !0,
            arrows: !1,
            infinite: !1,
            event: {
                afterChange: function (event, slick, currentSlide, nextSlide) {
                    $scope.videoReviewCurrentIndex = currentSlide
                },
                init: function (event, slick) {
                    slick.slickGoTo($scope.videoReviewCurrentIndex)
                }
            }
        }, $scope.photoSlickConfig = {
            enabled: !0,
            autoplay: !0,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            dots: !0,
            arrows: !1,
            infinite: !1,
            event: {
                afterChange: function (event, slick, currentSlide, nextSlide) {
                    $scope.photoCurrentIndex = currentSlide
                },
                init: function (event, slick) {
                    slick.slickGoTo($scope.photoCurrentIndex)
                }
            }
        }, $scope.videoSlickConfig = {
            enabled: !0,
            autoplay: !1,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            dots: !0,
            arrows: !1,
            infinite: !1,
            event: {
                afterChange: function (event, slick, currentSlide, nextSlide) {
                    $scope.videoCurrentIndex = currentSlide
                },
                init: function (event, slick) {
                    slick.slickGoTo($scope.videoCurrentIndex)
                }
            }
        }, $scope.trendingSlickConfig = {
            enabled: !0,
            autoplay: !1,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            dots: !0,
            arrows: !1,
            infinite: !1,
            event: {
                afterChange: function (event, slick, currentSlide, nextSlide) {
                    $scope.trendingCurrentIndex = currentSlide
                },
                init: function (event, slick) {
                    slick.slickGoTo($scope.trendingCurrentIndex)
                }
            }
        }, $scope.slidesData instanceof Array ? $scope.skip && $scope.slidesData.length > $scope.skip ? ($scope.slidesData.splice(0, $scope.skip), $scope.slides = $scope.slidesData) : $scope.slides = $scope.slidesData : $scope.slides.push($scope.slidesData), $scope.openArticle = function (section) {
            var _title = section.slug;
            $state.go("article", {
                category: section.mainSection.parentSlug,
                subcategory: section.mainSection.slug,
                title: _title,
                id: section._id
            })
        }, $scope.openGallery = function (photoID) {
            $state.go("photo", {
                id: photoID
            })
        }
    }]), app.controller("baseCtrl", ["$rootScope", "$state", "Storage", "APIService", "$timeout", "$sce", "$location", "$window","$filter", function ($rootScope, $state, Storage, APIService, $timeout, $sce, $location, $window,$filter) {
  
        $('body').on('click','img',function(){

            if($(this).attr('src').indexOf('static.spotboye')!=-1&&$(this).attr('src').indexOf('placeholder_mobile')==-1){

                if(!$(this).attr('no-popup')){
                    window.open($(this).attr('src'));
                }
            }
            });
            
       
        user_info = Storage.getCurrentUser(), $rootScope.getTime = function () {
            return (new Date).getTime()
        }, $rootScope.accent_color = "#111111", $rootScope.$on("changeAccentColor", function (_i, _p) {
            $rootScope.accent_color = _p.color
        }), $rootScope.openProfile = function () {
            Storage.getCurrentUser().islogged ? $state.go("profile") : $location.url("/login?redirect=profile")
        }, $rootScope.menus = "", APIService.getMenus(function (menus) {
            $rootScope.menus = menus
        }), $rootScope.loginRequire = function (url) {
            if (user_info = Storage.getCurrentUser(), !user_info.islogged) {
                var _r = url ? "?redirect=" + url : "";
                // $location.path("/login" + _r)
                $window.location.href = "/login" + _r
            }
        }, $rootScope.search = {}, $rootScope.search.keyword = "", $rootScope.search.find = function () {

            $rootScope.search.keyword.trim().length > 0 && ($state.go("search", {
                q: $rootScope.search.keyword
            }), $rootScope.search.keyword = "", closeNav())
        }, $rootScope.ui = {}, $rootScope.ui.backdrop = {}, $rootScope.ui.backdrop.visible = !1, $rootScope.ui.backdrop._url = "", $rootScope.ui.backdrop._html = "", $rootScope.ui.backdrop.data = {}, $rootScope.ui.backdrop.load = function (_d) {
            $rootScope.ui.backdrop.visible = !0, $rootScope.ui.backdrop._url = _d.url, $rootScope.ui.backdrop.data = _d.param
        }, $rootScope.ui.backdrop.loadHTML = function (_d) {
            $rootScope.ui.backdrop.visible = !0, $rootScope.ui.backdrop._html = $sce.trustAsHtml(_d.data), $rootScope.ui.backdrop.data = _d.param
        }, $rootScope.ui.backdrop.show = function () {
            $rootScope.ui.backdrop.visible = !0
        }, $rootScope.ui.backdrop.hide = function (clear) {
            clear && ($rootScope.ui.backdrop._html = "", $rootScope.ui.backdrop._url = ""), $rootScope.ui.backdrop.visible = !1
        }, $rootScope.$on("hideUiBackdrop", $rootScope.ui.backdrop.hide), $rootScope.$on("showUiBackdrop", function (_obj, _param) {
            "url" == _param.type ? $rootScope.ui.backdrop.load(_param) : "html" == _param.type && $rootScope.ui.backdrop.loadHTML(_param)
        }), $rootScope.ui.toolbar = {}, $rootScope.ui.toolbar.visible = !0, $rootScope.ui.toolbar.show = function () {
            $rootScope.ui.toolbar.visible = !0
        }, $rootScope.ui.toolbar.hide = function () {
            $rootScope.ui.toolbar.visible = !1
        }, $rootScope.ui.loader = {}, $rootScope.ui.loader.isVisible = !1, $rootScope.ui.loader.show = function () {
            $rootScope.ui.loader.isVisible = !0, $rootScope.$broadcast("onUiLoaderVisible")
        }, $rootScope.ui.loader.hide = function () {
            $rootScope.ui.loader.isVisible = !1, $rootScope.$broadcast("onUiLoaderClose")
        }, $rootScope.$on("showUiLoader", $rootScope.ui.loader.show), $rootScope.$on("hideUiLoader", $rootScope.ui.loader.hide), $rootScope.$on("$stateChangeStart", function () {
            $window.scrollTo(0, 0), $rootScope.ui.loader.show()
        }), $rootScope.$on("$viewContentLoaded", function () {
            $rootScope.ui.loader.hide(), $window.scrollTo(0, 0)
        }), $rootScope.ui.messagebox = {}, $rootScope.ui.messagebox.show = !1, $rootScope.ui.messagebox.title = "", $rootScope.ui.messagebox.message = "", $rootScope.ui.messagebox.clear = function () {
            $rootScope.ui.messagebox.show = !1, $rootScope.ui.messagebox.title = "", $rootScope.ui.messagebox.message = ""
        }, $rootScope.$on("closeMessagebox", $rootScope.ui.messagebox.clear), $rootScope.$on("showMessagebox", function (_obj, _param) {
            $rootScope.ui.messagebox.show = !0, $rootScope.ui.messagebox.title = _param.title ? _param.title : "", $rootScope.ui.messagebox.message = _param.message ? _param.message : "", $timeout($rootScope.ui.messagebox.clear, 3e3)
        }), $rootScope.Social = {
            fb: {}
        }, $rootScope.Social.fb.comment = function (url) {
            $rootScope.$emit("showUiBackdrop", {
                type: "html",
                data: "<iframe scrolling='yes' style='background:#fff;width:100%;height:100%' src='https://www.facebook.com/plugins/feedback.php?href=" + url + "'></iframe>"
            })
        }, $rootScope.Share = {}, $rootScope.Share.fb = function (url) {

            FB.ui({
                method: "share",
                display: "popup",
                href: $rootScope.localeURL+url
            }, function (response) {})
        }, $rootScope.$on("metaUpdate", function (_i, _param) {

            $rootScope.meta = _param
        })
    }]);
     app.controller("homeCtrl", ["$rootScope", "$scope", "APIService", "$stateParams", "$state", 
     function ($rootScope,$scope, APIService, $stateParams, $state) {
         
       

        var _meta = $stateParams.meta;
        _meta.thumbnail = "", _meta.pageURL = "https://www.spotboye.com",
         $scope.$emit("metaUpdate", _meta), 
         $scope._random = function () {
            $scope._r = Math.floor(100 * Math.random()) + 1
        }, $scope.loaded = !1, $scope.lists = [], $scope.$emit("showUiLoader");
         APIService.getDashboard(function (_lists) {
  
            $scope.lists = _lists, $scope.loaded = !0, $scope.$emit("hideUiLoader")
        }), $scope.user_info = user_info
    }]);
    app.controller("profileCtrl", ["$scope", "APIService", "$rootScope", "Storage", "$stateParams", "$window", function ($scope, APIService, $rootScope, Storage, $stateParams, $window) {
        user_info.islogged || ($window.location.href = "");
        var _meta = $stateParams.meta;
        _meta.thumbnail = "", _meta.pageURL = "https://www.spotboye.com/profile", $scope.$emit("metaUpdate", _meta), $rootScope.showToolbar = !1, $scope.profile = {}, $rootScope.loginRequire("profile");
        var _param = Storage.getCurrentUser();
        _param && _param.userId && APIService.getMyProfile(_param, function (_p) {
            $scope.profile = _p
        })
    }]), app.controller("articleCtrl", ["$scope", "APIService", "$stateParams", "$sce", function ($scope, APIService, $stateParams, $sce) {
        var last_article = 0;
        $scope.more = {}, $scope.more.articles = [], $scope.more.displat_txt = "";
        var last_article = {
            id: 0,
            catId: 0
        };
        $scope.agent = navigator.userAgent;
        $scope._random = function () {
            $scope._r = Math.floor(100 * Math.random()) + 1
        };
         jQuery(document).scroll(function () {
            $(".related_news").isOnScreen() || $(".more_articles").isOnScreen() ? $(".article_footer").hide() : $(".article_footer").show()
        }), $scope.slickConfig = {
            enabled: !0,
            autoplay: !0,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            event: {}
        }, $scope.ui = {}, $scope.ui.accent_color = "#444";
        $scope.ui.isLoaded = false;
         $scope.ui.like = function (_unlike) {
            if (user_info.islogged) {
                var _param = {
                    articleId: $scope.article._id,
                    userId: user_info.userId
                };
                APIService.likeArticle(_param, function (_d) {
                    _d.success && ($scope.article.userLike = !$scope.article.userLike, $scope.article.userLike ? $scope.article.likesCount++ : $scope.article.likesCount--)
                })
            } else $scope.$emit("showMessagebox", {
                title: "LOGIN",
                message: "You need to login before continue"
            });
        }, $scope.article = {}, $scope.displat_txt = {}, last_article.id = $stateParams.id;

        APIService.getArticle({
            article_id: last_article.id,
            userId: user_info.userId
        }, function (_article) {


            $scope.article = _article;
             $scope.article.body = $sce.trustAsHtml(_article.body);
             $scope.article.youtubeId = (_article.youtubeId && _article.youtubeId != '') ? $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + _article.youtubeId):'';
             $scope.ui.accent_color = $scope.article.mainSection.color, $scope.$emit("changeAccentColor", {
                color: $scope.article.mainSection.color
            }), last_article.catId = _article.mainSection._id, 
            $scope.ui.isLoaded = true;
             $scope.$emit("metaUpdate", {
                title: _article.title,
                description: _article.shortBody,
                thumbnail: _article.thumbnail.replace("http:","https:"),
                news_keyword: "",
                pageURL: window.location.href
            });
            var _breadcrumb = {};
            _breadcrumb["@context"] = "http://schema.org", _breadcrumb["@type"] = "BreadcrumbList", _breadcrumb.itemListElement = [{
                "@type": "ListItem",
                position: 1,
                item: {
                    "@id": "https://www.spotboye.com/" + $scope.article.mainSection.parentSlug,
                    name: $scope.article.mainSection.parentSection
                }
            }, {
                "@type": "ListItem",
                position: 2,
                item: {
                    "@id": "https://www.spotboye.com/" + $scope.article.mainSection.parentSlug + "/" + $scope.article.mainSection.slug,
                    name: $scope.article.mainSection.title
                }
            }], $("head").append("<script type='application/ld+json'>" + JSON.stringify(_breadcrumb) + "<\/script>");
            var _news_schema = {
                "@context": "http://schema.org",
                "@type": "NewsArticle",
                headline: $scope.article.title,
                image: {
                    "@type": "ImageObject",
                    url: $scope.article.thumbnail.replace("http:","https:"),
                    height: 500,
                    width: 800
                },
                datePublished: $scope.article.publishedAt_gmt,
                author: {
                    "@type": "Person",
                    name: $scope.article.authorName
                },
                publisher: {
                    "@type": "Organization",
                    name: "Spotboye",
                    logo: {
                        "@type": "ImageObject",
                        url: "https://www.spotboye.com/images/logo.png",
                        width: 180,
                        height: 40
                    }
                },
                description: $scope.article.shortBody
            };
            $("head").append("<script type='application/ld+json'>" + JSON.stringify(_news_schema) + "<\/script>");


        }), $scope.getNextArticle = function () {
            0 != last_article.id ? ($scope.more.displat_txt = "Loading next article..", APIService.getNextArticle(last_article, function (data) {
                data._id ? ($scope.more.displat_txt = "", last_article.id = data._id, last_article.catId = data.mainSection._id, data.body = $sce.trustAsHtml(data.body), $scope.more.articles.push(data), didScroll = !0) : $scope.more.displat_txt = ""
            })) : $scope.more.displat_txt = "Unable to retrieve more info"
        }
    }]),

    app.controller("articlesCtrl", ["$scope", "APIService", "$stateParams", "$sce", "$location", "$filter", "$window", "$state", "$filter", "$compile", "$timeout", function ($scope, APIService, $stateParams, $sce, $location, $filter, $window, $state, $filter, $compile, $timeout) {


        $scope._random = function () {
            $scope._r = Math.floor(100 * Math.random()) + 1
        };

        $scope.slickConfig = {
            enabled: !0,
            autoplay: !0,
            draggable: !0,
            autoplaySpeed: 3e3,
            method: {},
            event: {}
        };



        $scope.insertRelatedArticle = function () {

            if ($('.articleBody').find('.moreNewsBetween').length == 0) {

                var limited_rel_item = ($scope.article.related.length > 3) ? $scope.article.related.slice(0, 3) : $scope.article.related;
                if (limited_rel_item.length > 0) {
                    var _h = "<div id='InRead'></div><google-ads ng-init='_random()' id='mid_ads_article_{{_r}}' type='AP_300x250_Mid' class='matchedAdHeight'></google-ads> <div class='moreNewsBetween'><h2>STORIES YOU SHOULD NOT MISS</h2><ul>";
                    limited_rel_item.forEach(function (_art) {
                        //add link and limit this to 3;
                        _h += "<li><a href='/" + _art.mainSection.parentSlug + "/" + _art.mainSection.slug + "/" + $filter('slug')(_art.title) + "/" + _art._id + "'>" + _art.title + "</a></li>";
                    });
                    _h += "</ul></div>";
                }
                var _ad = $compile(angular.element(_h))($scope);
                if ($('.articleBody').find('p').contents().filter('br:eq(3)').length > 0) {
                    // _elem=$('.articleBody').find('.MsoNoSpacing').contents().filter('br:eq(3)');

                    $(_ad).insertBefore($('.articleBody').find('.MsoNoSpacing').contents().filter('br:eq(3)'));
                } else {
                    //  _elem=$('.articleBody').find('p:eq(2)');
                    if ($('.articleBody').find('p:eq(2)').length > 0) {

                        $(_ad).insertBefore($('.articleBody').find('p:eq(2)'))
                    } else {

                        $(_ad).insertBefore($('.articleBody').find('div:eq(2)'))
                    }

                }

            }


        }
        var _sI = setInterval(function () {

            if ($('.articleBody').length > 0) {
                $scope.insertRelatedArticle();
                clearInterval(_sI);
            }

        }, 500);


        APIService.getArticle({
            article_id: $stateParams.id,
            userId: user_info.userId
        }, function (_article) {

            if (_article.mainSection.parentSlug == "fashion") {

                $timeout(function () {

                    var _elem = angular.element(".articleBody").eq(0);
                    angular.forEach(_elem.find('img'), function (elem, _i) {
                        if (_i > 0) {

                            var _e = $compile(angular.element('<staqu-ad src="' + elem.src + '"></staqu-ad>'))($scope);
                            angular.element(".articleBody").eq(0).find('img').eq(_i).after(_e);
                        }
                    });
                }, 200);
            }

            //YOUTUBE
            _article.showYoutube = false;
            if (_article.type == 'Video') {
                _article.showYoutube = true;
                _article.playerURL = $sce.trustAsResourceUrl("//www.youtube.com/embed/" + _article.youtubeId);
            }

            var article = _article;

            article.body = $sce.trustAsHtml(_article.body);
            $scope.article = article;
            $scope.article.loaded = true;

            // article.loaded = !0, $scope.items.articles.push(article), $scope.current.index = 0, _article.nextArticles && _article.nextArticles.length > 0 && "Photo" !== _article.type && _article.nextArticles.forEach(function (_a, _i) {
            //    _a.loaded = !1;
              //   $scope.items.articles.push(_a);
            //})
             $scope.ui.accent_color = _article.mainSection.color, $scope.$emit("changeAccentColor", {
                color: _article.mainSection.color
            });
             $scope.ui.isLoaded = true;
             $scope.$emit("metaUpdate", {
                title: _article.title,
                description: _article.shortBody,
                thumbnail: _article.thumbnail.replace("http:","https:"),
                news_keyword: "",
                pageURL: window.location.href
            });






            // clevertap.event.push("Mobile Article Page Loaded", {
            //     //              "Article Link":window.location.href,
            //     "Article Title": _article.title
            // });
            var _breadcrumb = {};
            _breadcrumb["@context"] = "http://schema.org", _breadcrumb["@type"] = "BreadcrumbList", _breadcrumb.itemListElement = [{
                "@type": "ListItem",
                position: 1,
                item: {
                    "@id": "https://www.spotboye.com/" + _article.mainSection.parentSlug,
                    name: _article.mainSection.parentSection
                }
            }, {
                "@type": "ListItem",
                position: 2,
                item: {
                    "@id": "https://www.spotboye.com/" + _article.mainSection.parentSlug + "/" + _article.mainSection.slug,
                    name: _article.mainSection.title
                }
            }], $("head").append("<script type='application/ld+json'>" + JSON.stringify(_breadcrumb) + "<\/script>");
            var _news_schema = {
                "@context": "http://schema.org",
                "@type": "NewsArticle",
                headline: _article.title,
                image: {
                    "@type": "ImageObject",
                    url: _article.thumbnail.replace("http:","https:"),
                    height: 500,
                    width: 800
                },
                datePublished: _article.publishedAt_gmt,
                author: {
                    "@type": "Person",
                    name: _article.authorName
                },
                publisher: {
                    "@type": "Organization",
                    name: "Spotboye",
                    logo: {
                        "@type": "ImageObject",
                        url: "https://www.spotboye.com/images/logo.png",
                        width: 180,
                        height: 40
                    }
                },
                description: _article.shortBody
            };
            $("head").append("<script type='application/ld+json'>" + JSON.stringify(_news_schema) + "<\/script>");

        })
    }]);

app.controller('categoryCtrl', ['$rootScope', '$scope', 'APIService', '$stateParams', '$state', '$sce', '$filter', function ($rootScope, $scope, APIService, $stateParams, $state, $sce, $filter) {
    var page = 1;
    var limit = 20;

    $scope.loaded = false;
    $scope.category = {
        isParent: true,
        lists: [],
        maincategory: "",
        subcategory: ""
    };
    $scope.submenu = [];
    if ($stateParams.category && $stateParams.category.trim().length > 0) {
        $scope.category.maincategory = $stateParams.category;

    }
    if ($stateParams.subcategory && $stateParams.subcategory.trim().length > 0) {
        $scope.category.subcategory = $stateParams.subcategory;
        $scope.category.isParent = false;
    }
    if ($scope.category.isParent) {

        APIService.getByCategory($stateParams.category, function (_res) {
            //console.log(_res.data[0].data,"res");
            $scope.loaded = true;
            $scope.category.color = _res.color;
            //Change browser accent color
            $scope.$emit('changeAccentColor', {
                color: _res.color
            });

            if ($stateParams.category == 'time-pass') {
                //loop
                if (_res.data[0])
                var _mix = _res.data[0].data;
                //_mix = _mix.concat(_res.data[1].data, _res.data[2].data);
                _mix = _mix.concat(_res.data[1].data);

                $scope.category.lists = _mix;

            } else {
                $scope.category.lists = _res.data;

            }


            /** REFRESH META **/
            $scope.$emit('metaUpdate', {
                title: _res.metaTitle,
                description: _res.metaDescription,
                pageURL: window.location.href
            });

        });
    } else {

        $scope.paginateMore = function () {
            APIService.getBySubCategory({
                parent: $scope.category.maincategory,
                child: $scope.category.subcategory,
                page: page++,
                limit: limit
            }, function (_res) {
                $scope.category.color = _res.color;
                $scope.loaded = true;

                //Change browser accent color
                $scope.$emit('changeAccentColor', {
                    color: _res.color
                });


                if (_res.data.length > 0 && page - 1 == 1) {
                    $scope.category.lists = _res.data;
                } else {
                    $scope.category.lists.push.apply($scope.category.lists, _res.data);

                }
                /** REFRESH META **/
                if (page == 0) {
                    // clevertap.event.push("Mobile Secondary Menu Clicked", {
                    //     "Category": $scope.category.subcategory
                    // });
                    $scope.$emit('metaUpdate', {
                        title: _res.metaTitle,
                        description: _res.metaDescription,
                        pageURL: window.location.href
                    });
                } else {
                    // clevertap.event.push("Mobile Sub-Category Load more", {
                    //     "Category": $scope.category.subcategory,
                    //     "Page": page - 1,
                    //     "Limit": limit
                    // });
                }
            });
        }

        $scope.paginateMore(); //init on load
    }

    $scope.openPost = function (section) {
        if (section.type == "Video") {
            $rootScope.openVideo(section);
        } else {
            var _title = $filter('slug')(section.title);
            $state.go('article', {
                category: section.mainSection.parentSlug,
                subcategory: section.mainSection.slug,
                title: _title,
                id: section._id
            });
        }
    }
}]);



app.controller('loginCtrl', ['$rootScope', '$scope', 'APIService', '$stateParams', '$location', 'Storage', function ($rootScope, $scope, APIService, $stateParams, $location, Storage) {
    $scope.display = {
        container: "login",
        txt: {
            login: "",
            signup: "",
            otp: "",
            reset_pwd: ""
        }
    }; //show login by default
    /** REFRESH META **/
    $scope.$emit('metaUpdate', {
        title: "Login",
        description: "Login to start using spotboye",
        pageURL: window.location.href
    });

    $scope.sign = { in: {},
        up: {}
    };
    $scope.validateEmail = function (_e) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(_e);
    }
    $scope.sign.up.submit = function () {
        var _params = {
            name: $scope.sign.up.name,
            email: $scope.sign.up.email,
            password: $scope.sign.up.password,
            phone: $scope.sign.up.phone,
        }
        if (!$scope.sign.up.name || $scope.sign.up.name == "") {
            $scope.display.txt.signup = "PLEASE ENTER YOUR FULLNAME";
        } else if (!$scope.sign.up.phone || !(/^(\+91-|\+91|0)?\d{10}$/).test($scope.sign.up.phone)) {
            $scope.display.txt.signup = "PLEASE ENTER VALID MOBILE NUMBER";
        } else if (!$scope.validateEmail($scope.sign.up.email)) {
            $scope.display.txt.signup = "EMAIL ID IS INVALID";
        } else if (!$scope.sign.up.password || $scope.sign.up.password == "") {
            $scope.display.txt.signup = "PLEASE ENTER YOUR PASSWORD";
        } else {
            APIService.signup(_params, function (res) {
                if (res.success) {
                    $scope.display.container = "otp";
                } else {
                    $scope.display.txt.signup = res.message;
                }
            });
        }

    }
    $scope.verify = {};
    $scope.verify.code = "";
    $scope.verify.submit = function () {
        var _params = {
            verificationCode: $scope.verify.code
        }
        APIService.verifyOTP(_params, function (res) {
            if (res.success) {
                if (res.success) {
                    user_info = Storage.getCurrentUser();
                    var _qry = $location.search();
                    if (_qry.redirect) {
                        $location.path(_qry.redirect);
                    } else {
                        $location.path("/");
                    }
                } else {
                    $scope.display.txt.otp = res.message;
                }
            } else {
                $scope.display.txt.otp = "Unable to verify OTP";
            }
        });
    }
    /** RESET PSWD***/
    $scope.reset = {};
    $scope.reset.email = "";
    $scope.reset.submit = function () {
        var _params = {
            email: $scope.reset.email
        }

        if (!$scope.validateEmail($scope.reset.email)) {
            $scope.display.txt.reset = "Invalid email id";
        } else {
            $scope.display.txt.reset = "Please wait..";
            APIService.resetPassword(_params, function (res) {
                if (res.success) {
                    if (res.success) {
                        $scope.display.txt.reset = "New password has been sent to your email id.";
                    } else {
                        $scope.display.txt.reset = res.message;
                    }
                } else {
                    $scope.display.txt.reset = res.message;
                }
            });
        }
    }
    $scope.verify.resend = function () {
        var _params = {
            email: ($scope.sign.up.email && $scope.sign.up.email != "") ? $scope.sign.up.email : $scope.sign.in.email
        }
        $scope.display.txt.otp = "Please wait..";

        APIService.resendOTP(_params, function (res) {
            if (res.success) {
                if (res.success) {
                    $scope.display.txt.otp = "OTP Resent successfully";

                } else {
                    $scope.display.txt.otp = res.message;
                }
            } else {
                $scope.display.txt.otp = "Unable to verify OTP";
            }
        });
    }

    $scope.sign.in.submit = function () {
        var _params = {
            email: $scope.sign.in.email,
            password: $scope.sign.in.password
        }
        if (!$scope.validateEmail($scope.sign.in.email)) {
            $scope.display.txt.login = "EMAIL ID IS INVALID";
        } else if (!$scope.sign.in.password || $scope.sign.in.password == "") {
            $scope.display.txt.login = "PLEASE ENTER YOUR PASSWORD";
        } else {
            APIService.doLogin(_params, function (res) {
                $scope.$emit('hideUiBackdrop');
                if (res.success) {
                    user_info = Storage.getCurrentUser();
                    var _qry = $location.search();
                    if (_qry.redirect) {
                        $location.path("/" + _qry.redirect);
                    } else {
                        $location.path("/");
                    }
                } else {
                    if (res.isVerified) {
                        $scope.display.txt.login = res.message;
                    } else {
                        $scope.display.container = "otp";
                    }
                }
            });
        }
    }
    $rootScope.$on('event:social-sign-in-success', function (event, userDetails) {

        APIService.doFacebookLogin({
            access_token: userDetails.token
        }, function (res) {
            if (res.success) {
                user_info = Storage.getCurrentUser();
                var _qry = $location.search();
                if (_qry.redirect) {
                    $location.path("/" + _qry.redirect);
                } else {
                    $location.path("/");
                }
            } else {
                $scope.display.txt.login = res.message;
            }
        });
    });
}]);


app.controller('photoCtrl', ['$scope', 'APIService', '$stateParams', '$state', function ($scope, APIService, $stateParams, $state) {
    $scope.gallery = {};
    $scope.slickConfig = {
        enabled: true,
        autoplay: true,
        draggable: true,
        autoplaySpeed: 3000,
        method: {},
        event: {}
    };

    if ($stateParams.id) {
        APIService.getPhotoGallery($stateParams.id, function (data) {
            $scope.gallery = data;
            /** REFRESH META **/
            $scope.$emit('metaUpdate', {
                title: data.title,
                description: data.description,
                pageURL: window.location.href
            });
        });
    } else {
        $state.go("not-found");
    }
}]);

app.controller("celebrityCtrl", ["$scope", "APIService", "$stateParams", "$state", function ($scope, APIService, $stateParams, $state) {
    if ($scope.profile = {}, $stateParams.id) {
        var _p = {};
        user_info.islogged && (_p.userId = user_info.userId), _p.celebrityId = $stateParams.id, APIService.getCelebrity(_p, function (data) {
            // clevertap.event.push("Mobile celebrity Viewed", {
            //     "Celeb name": data.name
            // });
            data.success && ($scope.profile = data, $scope.$emit("metaUpdate", {
                title: data.name,
                description: data.name,
                pageURL: window.location.href
            }))
        }), $scope.setFollow = function (follow) {
            user_info.islogged ? APIService.setCelebrityFollow(_p, function (_r) {
                _r.success && ($scope.profile.follow = _r.following)
            }) : $scope.$emit("showMessagebox", {
                title: "LOGIN",
                message: "You need to login before continue"
            })
        }
    } else $state.go("not-found")
}]), app.controller("allCelebrityCtrl", ["$scope", "APIService", function ($scope, APIService) {
    $scope.list = {}, $scope.$emit("metaUpdate", {
        title: "Celebrities",
        description: "Find All Bollywood, Hollywood, Kollywood Celebrities Here",
        pageURL: window.location.href
    }), APIService.getCelebrities(function (data) {
        $scope.list = data
    }), $scope.search = {}, $scope.search.keyword = "", $scope.search.celebs = function () {
        APIService.findCelebs({
            name: $scope.search.keyword
        }, function (_d) {
            $scope.list.celebs = _d.celebs
        })
    }
}]), app.controller("searchCtrl", ["$scope", "$stateParams", "APIService", function ($scope, $stateParams, APIService) {
    if ($scope.search = {}, $stateParams.q) {
        var _keyword = $stateParams.q;
        APIService.search(_keyword, function (_result) {
            // clevertap.event.push("Mobile User Search", {
            //     "Search Keyword": _keyword
            // });
            $scope.$emit("metaUpdate", {
                title: "Search '" + _keyword + "'",
                description: "Search any article or celebrities",
                pageURL: window.location.href
            }), $scope.search = _result
        })
    }
}]), $.fn.isOnScreen = function () {
    var win = $(window),
        viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft()
        };
    viewport.right = viewport.left + win.width(), viewport.bottom = viewport.top + win.height();
    var bounds = this.offset();
    if (bounds) return bounds.right = bounds.left + this.outerWidth(), bounds.bottom = bounds.top + this.outerHeight(), !(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom)
};
